const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  "type": "mongodb",
  "url": process.env.MONGO_CONNECTION_STRING,
  "useNewUrlParser": true,
  "synchronize": true,
  "logging": true, 
  "migrations": ["dist/migrations/*.js"],
  "cli": {
    "migrationsDir": "dist/migrations"
  }
}