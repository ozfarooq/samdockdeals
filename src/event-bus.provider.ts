import {
  EventStoreBusConfig,
  EventStoreSubscriptionType,
} from 'nestjs-eventstore';
import * as DealEventInstantiators from '@daypaio/domain-events/deals/impl';
import * as ContactDraftsEventInstantiators from '@daypaio/domain-events/contact-drafts/impl';
import * as TenantEventInstantiators from '@daypaio/domain-events/tenants/impl';
import * as UserEventInstantiators from '@daypaio/domain-events/users/impl';

export const eventStoreBusConfig: EventStoreBusConfig = {
  subscriptions: [
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-deals',
      persistentSubscriptionName: 'deals',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-pipelines',
      persistentSubscriptionName: 'deals',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-contact_drafts',
      persistentSubscriptionName: 'deals',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-TenantDeletedEvent',
      persistentSubscriptionName: 'deals',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-UserAddedEvent',
      persistentSubscriptionName: 'deals',
    },
  ],
  events: {
    ...DealEventInstantiators,
    ...ContactDraftsEventInstantiators,
    ...TenantEventInstantiators,
    ...UserEventInstantiators,
  },
};
