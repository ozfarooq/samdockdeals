import { ApiProperty } from '@nestjs/swagger';

export class RemovePipelineStageDTO {
  @ApiProperty({
    description: 'Move all deals of removed stage to that stage id',
    required: false,
    default: null,
  })
  moveDealsTo?: string | null;
}
