import {
  IsString,
  IsOptional,
  IsBoolean,
} from 'class-validator';
import { plainToClassFromExist } from 'class-transformer';
import { IDeal, IDealPosition, IDealStage } from '@daypaio/domain-events/deals';
import { IUser } from '@daypaio/domain-events/users';
import { IOrganization } from '@daypaio/domain-events/organizations';

export class DealDTO implements IDeal {

  constructor(data?: any) {
    plainToClassFromExist(this, data);
  }

  // @IsString()
  _id: string;

  @IsString()
  @IsOptional()
  name: string;

  @IsOptional()
  organization: Partial<IOrganization>;

  @IsOptional()
  editor: IUser;

  @IsOptional()
  positions: IDealPosition[];

  @IsOptional()
  positionsEstimated: number;

  @IsOptional()
  stage: IDealStage;

  @IsOptional()
  stakeholders: string[];

  @IsOptional()
  contactPerson: string;

  @IsOptional()
  createdAt: number;

  @IsOptional()
  @IsBoolean()
  isArchived: boolean;

}
