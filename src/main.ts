import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setupDotenv } from './dotenv.setup';
import { Logger, ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const logger = new Logger('bootstrap');

  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const domainName = process.env.DOMAIN?.toLowerCase() || 'deals';
  app.setGlobalPrefix(`api/${domainName}`);

  const options = new DocumentBuilder()
    .setTitle(domainName)
    .setDescription(`${domainName} Microservice`)
    .setVersion('0.1.0')
    .setBasePath(`/api/${domainName}/`)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(`/api/${domainName}/docs`, app, document);
  app.enableCors();

  const port = process.env.PORT || 3000;
  await app.listen(port);

  logger.log(`Application listening on port ${port}`);
}

setupDotenv();
bootstrap();
