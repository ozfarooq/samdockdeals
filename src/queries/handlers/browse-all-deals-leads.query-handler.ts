import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { Deal } from '../../models/deal.model';
import { BrowseAllDealsLeadsQuery } from '../impl/browse-all-deals-leads.query';

@QueryHandler(BrowseAllDealsLeadsQuery)
export class BrowseAllDealsLeadsQueryHandler
  implements IQueryHandler<BrowseAllDealsLeadsQuery> {
  private logger: Logger;
  constructor(private repository: DealRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(query: BrowseAllDealsLeadsQuery): Promise<Deal[]> {
    this.logger.log('QUERY TRIGGERED: BrowseAllDealsLeadsQueryHandler...');
    try {
      return await this.repository.browseAll(query.meta);
    } catch (error) {
      this.logger.error('Failed to browse deals and leads');
      this.logger.log(error.stack);
      throw error;
    }
  }
}
