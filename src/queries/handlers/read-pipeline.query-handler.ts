import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ReadPipelineQuery } from '../impl/read-pipeline.query';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@QueryHandler(ReadPipelineQuery)
export class ReadPipelineQueryHandler
  implements IQueryHandler<ReadPipelineQuery> {
  private logger: Logger;
  constructor(private repository: PipelineRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(query: ReadPipelineQuery) {
    this.logger.log('QUERY TRIGGERED');
    try {
      return await this.repository.read(query.id, query.meta);
    } catch (error) {
      this.logger.error('QUERY FAILED');
      this.logger.log(error.stack);
      throw error;
    }
  }
}
