import { ReadDealHandler } from './read-deal.query-handler';
import { BrowseDealsHandler } from './browse-deals.query-handler';
import { BrowsePipelinesQueryHandler } from './browse-pipelines.query-handler';
import { BrowseLeadsQueryHandler } from './browse-leads.query-handlers';
import { BrowseAllDealsLeadsQueryHandler } from './browse-all-deals-leads.query-handler';
import { ReadPipelineQueryHandler } from './read-pipeline.query-handler';

export const DealQueryHandlers = [
  ReadDealHandler,
  BrowseDealsHandler,
  BrowseLeadsQueryHandler,
  BrowsePipelinesQueryHandler,
  BrowseAllDealsLeadsQueryHandler,
  ReadPipelineQueryHandler,
];
