import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { BrowsePipelinesQuery } from '../impl/browse-pipelines.query';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@QueryHandler(BrowsePipelinesQuery)
export class BrowsePipelinesQueryHandler
  implements IQueryHandler<BrowsePipelinesQuery> {
  private logger: Logger;
  constructor(private repository: PipelineRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(query: BrowsePipelinesQuery) {
    this.logger.log('QUERY TRIGGERED');
    try {
      return await this.repository.browse(query.meta);
    } catch (error) {
      this.logger.error('QUERY FAILED');
      this.logger.log(error.stack);
      throw error;
    }
  }
}
