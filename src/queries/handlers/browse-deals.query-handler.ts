import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { BrowseDealsQuery } from '../impl/browse-deals.query';
import { DealRepository } from '../../repositories/deals.repository';
import { Deal } from '../../models/deal.model';

@QueryHandler(BrowseDealsQuery)
export class BrowseDealsHandler
  implements IQueryHandler<BrowseDealsQuery> {
  private logger: Logger;
  constructor(private repository: DealRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(query: BrowseDealsQuery): Promise<Deal[]> {
    this.logger.log('QUERY TRIGGERED: BrowseDealsHandler...');
    try {
      return await this.repository.browseDeals(query.meta);
    } catch (error) {
      this.logger.error('Failed to browse deals');
      this.logger.log(error.stack);
      throw error;
    }
  }
}
