import { TestingModule, Test } from '@nestjs/testing';
import { CqrsModule, QueryBus } from '@nestjs/cqrs';
import { DealQueryHandlers } from '.';
import { DealRepository } from '../../repositories/deals.repository';
import { BrowseDealsQuery } from '../impl/browse-deals.query';
import { BrowseDealsHandler } from './browse-deals.query-handler';
import { Deal } from '../../models/deal.model';
import { NotFoundException } from '@nestjs/common';
import { ReadDealHandler } from './read-deal.query-handler';
import { ReadDealQuery } from '../impl/read-deal.query';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { PipelineRepository } from '../../repositories/pipeline.repository';

const mockDealRepository = () => ({
  read: jest.fn(),
  browse: jest.fn(),
  browseLeads: jest.fn(),
  browseDeals: jest.fn(),
});

const mockMeta = new EventMetaData('mocktenant', 'mock123');

function createMockDeals(): Deal[] {
  const firstDeal = new Deal({
    _id: 'org1',
    name: 'T',
  });

  const secondDeal = new Deal({
    _id: 'org2',
    name: 'J',
  });

  const arrayOfDeals: Deal[] = [];
  arrayOfDeals.push(firstDeal);
  arrayOfDeals.push(secondDeal);

  return arrayOfDeals;
}

describe('DealQueryHandlers', () => {
  const mockDeals: Deal[] = createMockDeals();
  let queryBus: QueryBus;
  let repo;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ...DealQueryHandlers,
        {
          provide: DealRepository,
          useFactory: mockDealRepository,
        },
        {
          provide: PipelineRepository,
          useFactory: mockDealRepository,
        },
      ],
    }).compile();
    queryBus = module.get<QueryBus>(QueryBus);
    repo = module.get<DealRepository>(DealRepository);
  });

  describe(' for BrowseQueryHandler', () => {
    it('should successfully return an array of deals', async () => {
      repo.browseDeals.mockResolvedValue(mockDeals);
      queryBus.register([BrowseDealsHandler]);
      expect(repo.browseDeals).not.toHaveBeenCalled();
      const result = await queryBus.execute(
        new BrowseDealsQuery(mockMeta),
      );
      expect(repo.browseDeals).toHaveBeenCalledWith(mockMeta);
      expect(result).toEqual(mockDeals);
    });

    it('should throw InternatServerException on the failure of repository.browse()', async () => {
      repo.browseDeals.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      queryBus.register([BrowseDealsHandler]);
      expect(repo.browseDeals).not.toHaveBeenCalled();
      expect(
        queryBus.execute(new BrowseDealsQuery(mockMeta)),
      ).rejects.toThrow(Error);
      expect(repo.browseDeals).toHaveBeenCalledWith(mockMeta);
    });
  });

  describe(' for ReadQueryHandler', () => {
    it('should successfully return an deal on repo.read()', async () => {
      repo.read.mockResolvedValue(mockDeals[0]);
      queryBus.register([ReadDealHandler]);
      expect(repo.read).not.toHaveBeenCalled();
      const result = await queryBus.execute(
        new ReadDealQuery(mockDeals[0]._id, mockMeta),
      );
      expect(repo.read).toHaveBeenCalledWith(mockDeals[0]._id, mockMeta);
      expect(result).toEqual(mockDeals[0]);
    });

    it('should throw NotFoundException on repo.read()', async () => {
      repo.read.mockReturnValue(
        Promise.reject(
          new NotFoundException('This is an auto generated error'),
        ),
      );
      queryBus.register([ReadDealHandler]);
      expect(repo.read).not.toHaveBeenCalled();
      expect(
        queryBus.execute(
          new ReadDealQuery(mockDeals[0]._id, mockMeta),
        ),
      ).rejects.toThrow(NotFoundException);
      expect(repo.read).toHaveBeenCalledWith(mockDeals[0]._id, mockMeta);
    });

    it('should throw InternalServerException on repo.read() failing', async () => {
      repo.read.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      queryBus.register([ReadDealHandler]);
      expect(repo.read).not.toHaveBeenCalled();
      expect(
        queryBus.execute(
          new ReadDealQuery(mockDeals[0]._id, mockMeta),
        ),
      ).rejects.toThrow(Error);
      expect(repo.read).toHaveBeenCalledWith(mockDeals[0]._id, mockMeta);
    });
  });
});
