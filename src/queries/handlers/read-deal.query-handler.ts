import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ReadDealQuery } from '../impl/read-deal.query';
import { DealRepository } from '../../repositories/deals.repository';

@QueryHandler(ReadDealQuery)
export class ReadDealHandler
  implements IQueryHandler<ReadDealQuery> {
  private logger: Logger;
  constructor(private repository: DealRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(query: ReadDealQuery) {
    this.logger.log('QUERY TRIGGERED: ReadDealHandler...');

    const { id, meta } = query;
    try {
      const result = await this.repository.read(id, meta);
      return result;
    } catch (error) {
      this.logger.error(`Failed to read deal of id ${id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
