import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { Deal } from '../../models/deal.model';
import { BrowseLeadsQuery } from '../impl/browse-leads.query';

@QueryHandler(BrowseLeadsQuery)
export class BrowseLeadsQueryHandler
  implements IQueryHandler<BrowseLeadsQuery> {
  private logger: Logger;
  constructor(private repository: DealRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(query: BrowseLeadsQuery): Promise<Deal[]> {
    this.logger.log('QUERY TRIGGERED: BrowseLeadsQueryHandler...');
    try {
      return await this.repository.browseLeads(query.meta);
    } catch (error) {
      this.logger.error('Failed to browse deals');
      this.logger.log(error.stack);
      throw error;
    }
  }
}
