import { IQuery } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ReadPipelineQuery implements IQuery {
  constructor(public id: string, public meta: EventMetaData) {}
}
