import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DecisionMakerRemovedFromDealEvent, DecisionMakerRemovedFromLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(DecisionMakerRemovedFromDealEvent, DecisionMakerRemovedFromLeadEvent)
export class DecisionMakerRemovedFromDealEventHandler implements
    IEventHandler<DecisionMakerRemovedFromDealEvent | DecisionMakerRemovedFromLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: DecisionMakerRemovedFromDealEvent | DecisionMakerRemovedFromLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, meta } = event;
    try {
      await this.repository.removeDecisionMaker(_dealID, meta);

    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
