import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { LeadReopenedEvent } from '@daypaio/domain-events/deals';

@EventsHandler(LeadReopenedEvent)
export class LeadReopenedEventHandler implements IEventHandler<LeadReopenedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: LeadReopenedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _leadID, meta } = event;
    try {
      await this.repository.reopen(_leadID, meta);
    } catch (error) {
      this.logger.error(error);
    }
  }
}
