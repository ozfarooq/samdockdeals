import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { PersonUnrelatedFromDealEvent } from '@daypaio/domain-events/deals';

@EventsHandler(PersonUnrelatedFromDealEvent)
export class PersonUnrelatedFromDealEventHandler implements
    IEventHandler<PersonUnrelatedFromDealEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: PersonUnrelatedFromDealEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, _personID, meta } = event;
    try {
      await this.repository.unrelatePerson(_dealID, _personID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
