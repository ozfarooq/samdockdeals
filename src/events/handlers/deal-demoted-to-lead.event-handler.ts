import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DealDemotedToLeadEvent } from '@daypaio/domain-events/deals';
import { QUALIFIED_STAGE_ID } from '../../models/pipeline.model';

@EventsHandler(DealDemotedToLeadEvent)
export class DealDemotedToLeadEventHandler
  implements IEventHandler<DealDemotedToLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}
  async handle(event: DealDemotedToLeadEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _dealID, meta } = event;
    try {
      await this.repository.changeStage(
        _dealID,
        { stageID: QUALIFIED_STAGE_ID, pipelineID: null },
        meta,
      );
    } catch (error) {
      this.logger.error(error);
    }
  }
}
