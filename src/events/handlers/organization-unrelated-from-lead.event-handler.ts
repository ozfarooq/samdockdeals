import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { OrganizationUnrelatedFromLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(OrganizationUnrelatedFromLeadEvent)
export class OrganizationUnrelatedFromLeadEventHandler implements
    IEventHandler<OrganizationUnrelatedFromLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: OrganizationUnrelatedFromLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _leadID, _organizationID, meta } = event;
    try {
      await this.repository.unrelateOrganization(_leadID, _organizationID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
