import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { BudgetAllocatedForDealEvent, BudgetAllocatedForLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(BudgetAllocatedForDealEvent, BudgetAllocatedForLeadEvent)
export class BudgetAllocatedForDealEventHandler implements
  IEventHandler<BudgetAllocatedForDealEvent | BudgetAllocatedForLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) { }

  async handle(event: BudgetAllocatedForDealEvent | BudgetAllocatedForLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, budget, meta } = event;
    try {
      if (budget === null) {
        await this.repository.removeBudget(_dealID, meta);
      } else {
        await this.repository.assignBudget(_dealID, budget, meta);
      }

    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
