import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { OrganizationRelatedToLeadEvent } from '@daypaio/domain-events/deals';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';

@EventsHandler(OrganizationRelatedToLeadEvent)
export class OrganizationRelatedToLeadEventHandler implements
    IEventHandler<OrganizationRelatedToLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: OrganizationRelatedToLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _leadID, _organizationID, meta } = event;
    try {
      await this.repository.relateOrganization(_leadID, _organizationID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
