import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PipelineRepository } from '../../repositories/pipeline.repository';
import { DealSortedEvent } from '@daypaio/domain-events/deals';

@EventsHandler(DealSortedEvent)
export class DealSortedEventHandler implements IEventHandler<DealSortedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: PipelineRepository) {}

  async handle(event: DealSortedEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { dealID, pipelineID, stageID, sortIndex, meta } = event;
    try {
      await this.repository.sortDeal(pipelineID, dealID, stageID, sortIndex, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
