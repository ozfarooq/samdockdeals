import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { LeadUnqualifiedEvent } from '@daypaio/domain-events/deals';
import { PROSPECTING_STAGE_ID } from '../../models/pipeline.model';

@EventsHandler(LeadUnqualifiedEvent)
export class LeadUnqualifiedEventHandler implements
    IEventHandler<LeadUnqualifiedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: LeadUnqualifiedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _dealID, meta } = event;
    try {
      await this.repository.changeStage(
        _dealID,
        { stageID: PROSPECTING_STAGE_ID, pipelineID: null },
        meta,
      );
    } catch (error) {
      this.logger.error(error);
    }
  }
}
