import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DealReopenedEvent } from '@daypaio/domain-events/deals';

@EventsHandler(DealReopenedEvent)
export class DealReopenedEventHandler implements IEventHandler<DealReopenedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: DealReopenedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _dealID, meta } = event;
    try {
      await this.repository.reopen(_dealID, meta);
    } catch (error) {
      this.logger.error(error);
    }
  }
}
