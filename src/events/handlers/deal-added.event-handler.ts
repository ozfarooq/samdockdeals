import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DealAddedEvent, LeadAddedEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';
import { FIRST_STAGE_ID } from '../../models/pipeline.model';

@EventsHandler(DealAddedEvent, LeadAddedEvent)
export class DealAddedEventHandler
  implements IEventHandler<DealAddedEvent | LeadAddedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(
    private repository: DealRepository,
    private pipelineRepository: PipelineRepository,
  ) {}

  async handle(event: DealAddedEvent | LeadAddedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, deal, meta } = event;
    try {
      if (!deal?.stage) {
        deal.stage = { stageID: FIRST_STAGE_ID, pipelineID: meta._tenantID };
      }
      await this.repository.add(deal, meta);
      await this.pipelineRepository.sortDeal(
        deal.stage.pipelineID,
        _id,
        deal.stage.stageID,
        0,
        meta,
      );
    } catch (error) {
      this.logger.error(`Failed to create deal of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }

}
