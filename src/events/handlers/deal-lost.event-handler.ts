import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DealLostEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(DealLostEvent)
export class DealLostEventHandler
  implements IEventHandler<DealLostEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(
    private repository: DealRepository,
    private pipelineRepository: PipelineRepository,
  ) {}

  async handle(event: DealLostEvent): Promise<void> {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, stage, meta } = event;
    try {
      await this.pipelineRepository.sortDeal(stage.pipelineID, _dealID, stage.stageID, 0, meta);
      await this.repository.changeStage(_dealID, stage, meta);
    } catch (error) {
      this.logger.error(error);
    }
  }
}
