import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PipelineStageRenamedEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(PipelineStageRenamedEvent)
export class PipelineStageRenamedEventHandler implements IEventHandler<PipelineStageRenamedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: PipelineRepository) {}

  async handle(event: PipelineStageRenamedEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { pipelineID, id, newName, meta } = event;
    try {
      await this.repository.renamePipelineStage(pipelineID, id, newName, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
