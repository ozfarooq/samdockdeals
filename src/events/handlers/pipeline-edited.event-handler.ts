import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PipelineEditedEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(PipelineEditedEvent)
export class PipelineEditedEventHandler
  implements IEventHandler<PipelineEditedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PipelineRepository) {}
  async handle(event: PipelineEditedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _id, pipeline, meta } = event;
    try {
      await this.repository.edit(_id, pipeline, meta);
    } catch (error) {
      this.logger.error(`Failed to edit pipeline of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
