import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { OrganizationRelatedToDealEvent } from '@daypaio/domain-events/deals';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';

@EventsHandler(OrganizationRelatedToDealEvent)
export class OrganizationRelatedToDealEventHandler implements
    IEventHandler<OrganizationRelatedToDealEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: OrganizationRelatedToDealEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, _organizationID, meta } = event;
    try {
      await this.repository.relateOrganization(_dealID, _organizationID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
