import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PipelineDeletedEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';
import { DealRepository } from '../../repositories/deals.repository';

@EventsHandler(PipelineDeletedEvent)
export class PipelineDeletedEventHandler
  implements IEventHandler<PipelineDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PipelineRepository, private dealRepository: DealRepository) { }
  async handle(event: PipelineDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, toPipelineID, toStageID, meta } = event;
    try {
      await this.repository.delete(_id, toPipelineID, toStageID, meta);

      if (toPipelineID && toStageID) {
        await this.dealRepository.updateMany(
          { 'stage.pipelineID': _id },
          {
            'stage.pipelineID': toPipelineID,
            'stage.stageID': toStageID,
          },
          meta,
        );
      }
    } catch (error) {
      this.logger.error(`Failed to delete deal of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
