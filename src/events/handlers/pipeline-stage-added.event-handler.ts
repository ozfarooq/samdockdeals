import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PipelineStageAddedEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(PipelineStageAddedEvent)
export class PipelineStageAddedEventHandler implements IEventHandler<PipelineStageAddedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: PipelineRepository) {}

  async handle(event: PipelineStageAddedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { pipelineID, stage, meta } = event;
    try {
      await this.repository.addPipelineStage(pipelineID, stage, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
