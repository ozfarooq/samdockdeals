import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { UserAddedEvent } from '@daypaio/domain-events/users';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(UserAddedEvent)
export class UserAddedEventHandler
  implements IEventHandler<UserAddedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PipelineRepository) {}

  async handle(event: UserAddedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { user, meta } = event;
    try {
      await this.repository.addDefaultPipeline(user, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
