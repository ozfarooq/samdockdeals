import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { CustomerRequirementChangedForDealEvent, CustomerRequirementChangedForLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(CustomerRequirementChangedForDealEvent, CustomerRequirementChangedForLeadEvent)
export class CustomerRequirementChangedForDealEventHandler implements
  IEventHandler<CustomerRequirementChangedForDealEvent | CustomerRequirementChangedForLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) { }

  async handle(
    event: CustomerRequirementChangedForDealEvent | CustomerRequirementChangedForLeadEvent,
  ) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, customerRequirement, meta } = event;
    try {
      if (customerRequirement === null) {
        await this.repository.removeCustomerRequirement(_dealID, meta);
      } else {
        await this.repository.assignCustomerRequirement(_dealID, customerRequirement, meta);
      }

    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
