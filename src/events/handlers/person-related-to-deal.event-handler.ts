import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { PersonRelatedToDealEvent } from '@daypaio/domain-events/deals';

@EventsHandler(PersonRelatedToDealEvent)
export class PersonRelatedToDealEventHandler implements
    IEventHandler<PersonRelatedToDealEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: PersonRelatedToDealEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, _personID, meta } = event;
    try {
      await this.repository.relatePerson(_dealID, _personID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
