import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { LeadPromotedToDealEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';
import { FIRST_STAGE_ID } from '../../models/pipeline.model';

@EventsHandler(LeadPromotedToDealEvent)
export class LeadPromotedToDealEventHandler
  implements IEventHandler<LeadPromotedToDealEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(
    private repository: DealRepository,
    private pipelineRepository: PipelineRepository,
  ) {}

  async handle(event: LeadPromotedToDealEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _dealID, pipelineID, meta } = event;
    try {
      await this.pipelineRepository.sortDeal(pipelineID, _dealID, FIRST_STAGE_ID, 0, meta);
      await this.repository.changeStage(
        _dealID,
        { pipelineID, stageID: FIRST_STAGE_ID },
        meta,
      );
    } catch (error) {
      this.logger.error(error);
    }
  }
}
