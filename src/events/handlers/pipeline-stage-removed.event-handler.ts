import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PipelineStageRemovedEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';
import { DealRepository } from '../../repositories/deals.repository';

@EventsHandler(PipelineStageRemovedEvent)
export class PipelineStageRemovedEventHandler implements IEventHandler<PipelineStageRemovedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(
    private repository: PipelineRepository,
    private dealRepository: DealRepository,
  ) {}

  async handle(event: PipelineStageRemovedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { pipelineID, id, toStageID, meta } = event;
    try {
      await this.dealRepository.updateMany(
        { 'stage.stageID': id },
        { 'stage.stageID': toStageID },
        meta,
      );
      await this.repository.removePipelineStage(pipelineID, id, toStageID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
