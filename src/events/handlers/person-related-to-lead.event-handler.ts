import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { PersonRelatedToLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(PersonRelatedToLeadEvent)
export class PersonRelatedToLeadEventHandler implements
    IEventHandler<PersonRelatedToLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: PersonRelatedToLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _leadID, _personID, meta } = event;
    try {
      await this.repository.relatePerson(_leadID, _personID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
