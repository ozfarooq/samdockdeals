import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { LeadCreatedFromDraftEvent } from '@daypaio/domain-events/deals';

@EventsHandler(LeadCreatedFromDraftEvent)
export class LeadCreatedFromDraftEventHandler implements
    IEventHandler<LeadCreatedFromDraftEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: LeadCreatedFromDraftEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, meta } = event;
    try {
      await this.repository.add({
        _id: _dealID,
        _tenantID: meta._tenantID,
      },                        meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
