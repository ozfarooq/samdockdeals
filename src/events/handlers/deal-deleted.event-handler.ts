import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DealDeletedEvent, LeadDeletedEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(DealDeletedEvent, LeadDeletedEvent)
export class DealDeletedEventHandler
  implements IEventHandler<DealDeletedEvent | LeadDeletedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(
    private repository: DealRepository,
    private pipelineRepository: PipelineRepository,
  ) {}

  async handle(event: DealDeletedEvent | LeadDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;
    try {
      await this.pipelineRepository.sortDeal(null, _id, null, null, meta);
      await this.repository.delete(_id, meta);
    } catch (error) {
      this.logger.error(`Failed to delete deal of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }

}
