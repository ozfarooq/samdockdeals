import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { OrganizationUnrelatedFromDealEvent } from '@daypaio/domain-events/deals';

@EventsHandler(OrganizationUnrelatedFromDealEvent)
export class OrganizationUnrelatedFromDealEventHandler implements
    IEventHandler<OrganizationUnrelatedFromDealEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: OrganizationUnrelatedFromDealEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, _organizationID, meta } = event;
    try {
      await this.repository.unrelateOrganization(_dealID, _organizationID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
