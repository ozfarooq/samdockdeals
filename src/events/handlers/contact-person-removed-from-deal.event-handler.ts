import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { ContactPersonRemovedFromDealEvent, ContactPersonRemovedFromLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(ContactPersonRemovedFromDealEvent, ContactPersonRemovedFromLeadEvent)
export class ContactPersonRemovedFromDealEventHandler implements
    IEventHandler<ContactPersonRemovedFromDealEvent | ContactPersonRemovedFromLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: ContactPersonRemovedFromDealEvent | ContactPersonRemovedFromLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, _personID, meta } = event;
    try {
      await this.repository.removeContactPerson(_dealID, _personID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
