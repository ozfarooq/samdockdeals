import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DealArchivedEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(DealArchivedEvent)
export class DealArchivedEventHandler implements IEventHandler<DealArchivedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(
    private repository: DealRepository,
    private pipelineRepository: PipelineRepository,
  ) {}

  async handle(event: DealArchivedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _dealID, meta } = event;
    try {
      await this.pipelineRepository.sortDeal(undefined, _dealID, null, null, meta);
      await this.repository.archive(_dealID, meta);
    } catch (error) {
      this.logger.error(error);
    }
  }
}
