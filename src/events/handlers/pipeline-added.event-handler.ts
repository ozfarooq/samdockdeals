import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PipelineAddedEvent } from '@daypaio/domain-events/deals';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(PipelineAddedEvent)
export class PipelineAddedEventHandler
  implements IEventHandler<PipelineAddedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PipelineRepository) {}
  async handle(event: PipelineAddedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { pipeline, meta } = event;
    try {
      await this.repository.add(pipeline, meta);
    } catch (error) {
      this.logger.error(`Failed to edit pipeline of id ${pipeline._id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
