import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { TimingChangedForDealEvent, TimingChangedForLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(TimingChangedForDealEvent, TimingChangedForLeadEvent)
export class TimingChangedForDealEventHandler implements
    IEventHandler<TimingChangedForDealEvent | TimingChangedForLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: TimingChangedForDealEvent | TimingChangedForLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, timestamp, meta } = event;
    try {
      if (!timestamp) {
        await this.repository.removeTiming(_dealID, meta);
      } else {
        await this.repository.setTiming(_dealID, timestamp , meta);
      }

    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
