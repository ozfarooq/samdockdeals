import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { BudgetHolderAssignedToDealEvent, BudgetHolderAssignedToLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(BudgetHolderAssignedToDealEvent, BudgetHolderAssignedToLeadEvent)
export class BudgetHolderAssignedToDealEventHandler implements
    IEventHandler<BudgetHolderAssignedToDealEvent | BudgetHolderAssignedToLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: BudgetHolderAssignedToDealEvent | BudgetHolderAssignedToLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, budgetHolderID, meta } = event;
    try {
      await this.repository.assignBudgetHolder(_dealID, budgetHolderID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
