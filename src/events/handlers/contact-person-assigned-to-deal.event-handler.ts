import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { ContactPersonAssignedToDealEvent, ContactPersonAssignedToLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(ContactPersonAssignedToDealEvent, ContactPersonAssignedToLeadEvent)
export class ContactPersonAssignedToDealEventHandler implements
    IEventHandler<ContactPersonAssignedToDealEvent | ContactPersonAssignedToLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: ContactPersonAssignedToDealEvent | ContactPersonAssignedToLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, _personID, meta } = event;
    try {
      await this.repository.assignContactPerson(_dealID, _personID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
