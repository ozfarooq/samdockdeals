import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DecisionMakerAssignedToDealEvent, DecisionMakerAssignedToLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(DecisionMakerAssignedToDealEvent, DecisionMakerAssignedToLeadEvent)
export class DecisionMakerAssignedToDealEventHandler implements
    IEventHandler<DecisionMakerAssignedToDealEvent | DecisionMakerAssignedToLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: DecisionMakerAssignedToDealEvent | DecisionMakerAssignedToLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, decisionMakerID, meta } = event;
    try {
      await this.repository.assignDecisionMaker(_dealID, decisionMakerID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
