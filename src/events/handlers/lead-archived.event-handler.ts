import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { LeadArchivedEvent } from '@daypaio/domain-events/deals';

@EventsHandler(LeadArchivedEvent)
export class LeadArchivedEventHandler implements IEventHandler<LeadArchivedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: LeadArchivedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _leadID, meta } = event;
    try {
      await this.repository.archive(_leadID, meta);
    } catch (error) {
      this.logger.error(error);
    }
  }
}
