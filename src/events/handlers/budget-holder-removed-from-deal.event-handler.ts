import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { BudgetHolderRemovedFromDealEvent, BudgetHolderRemovedFromLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(BudgetHolderRemovedFromDealEvent, BudgetHolderRemovedFromLeadEvent)
export class BudgetHolderRemovedFromDealEventHandler implements
    IEventHandler<BudgetHolderRemovedFromDealEvent | BudgetHolderRemovedFromLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: BudgetHolderRemovedFromDealEvent | BudgetHolderRemovedFromLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _dealID, meta } = event;
    try {
      await this.repository.removeBudgetHolder(_dealID, meta);

    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
