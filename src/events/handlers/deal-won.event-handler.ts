import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DealWonEvent } from '@daypaio/domain-events/deals/';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(DealWonEvent)
export class DealWonEventHandler
  implements IEventHandler<DealWonEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(
    private repository: DealRepository,
    private pipelineRepository: PipelineRepository,
  ) {}

  async handle(event: DealWonEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _dealID, stage, meta } = event;
    try {
      await this.pipelineRepository.sortDeal(stage.pipelineID, _dealID, stage.stageID, 0, meta);
      await this.repository.changeStage(_dealID, stage, meta);
    } catch (error) {
      this.logger.error(error);
    }
  }
}
