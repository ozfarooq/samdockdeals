import { DealAddedEventHandler } from './deal-added.event-handler';
import { DealDeletedEventHandler } from './deal-deleted.event-handler';
import { DealEditedEventHandler } from './deal-edited.event-handler';
import { PipelineStageRenamedEventHandler } from './pipeline-stage-renamed.event-handler';
import {
  OrganizationRelatedToDealEventHandler,
} from './organization-related-to-deal.event-handler';
import { OrganizationUnrelatedFromDealEventHandler } from './organization-unrelated-from-deal.event-handler';
import { PersonRelatedToDealEventHandler } from './person-related-to-deal.event-handler';
import { PersonUnrelatedFromDealEventHandler } from './person-unrelated-from-deal.event-handler';
import { ContactPersonAssignedToDealEventHandler } from './contact-person-assigned-to-deal.event-handler';
import { ContactPersonRemovedFromDealEventHandler } from './contact-person-removed-from-deal.event-handler';
import { DealWonEventHandler } from './deal-won.event-handler';
import { DealLostEventHandler } from './deal-lost.event-handler';
import { DealStageChangedEventHandler } from './deal-stage-changed.event-handler';
import { LeadCreatedFromDraftEventHandler } from './lead-created-from-draft.event-handler';
import { BudgetAllocatedForDealEventHandler } from './budget-allocated-to-deal.event-handler';
import { CustomerRequirementChangedForDealEventHandler } from './custom-requirement-changed-for-deal.event-handler';
import { DecisionMakerAssignedToDealEventHandler } from './decision-maker-assigned-to-deal.event';
import { DecisionMakerRemovedFromDealEventHandler } from './decision-maker-removed-from-deal.event-handler';
import { TimingChangedForDealEventHandler } from './timing-changed-for-deal.event-handler';
import { BudgetHolderAssignedToDealEventHandler } from './budget-holder-assigned-to-deal.event-handler';
import { BudgetHolderRemovedFromDealEventHandler } from './budget-holder-removed-from-deal.event-handler';
import { LeadQualifiedEventHandler } from './lead-qualified.event-handler';
import { LeadPromotedToDealEventHandler } from './lead-promoted-to-deal.event-handler';
import { DealDemotedToLeadEventHandler } from './deal-demoted-to-lead.event-handler';
import { PersonRelatedToLeadEventHandler } from './person-related-to-lead.event-handler';
import { PersonUnrelatedFromLeadEventHandler } from './person-unrelated-from-lead.event-handler';
import {
  OrganizationRelatedToLeadEventHandler,
} from './organization-related-to-lead.event-handler';
import {
  OrganizationUnrelatedFromLeadEventHandler,
} from './organization-unrelated-from-lead.event-handler';
import { TenantDeletedEventHandler } from './tenant-deleted.event-handler';
import { PipelineDeletedEventHandler } from './pipeline-deleted.event-handler';
import { DealSortedEventHandler } from './deal-sorted.event-handler';
import { LeadUnqualifiedEventHandler } from './lead-unqualified.event-handler';
import { LeadArchivedEventHandler } from './lead-archived.event-handler';
import { DealArchivedEventHandler } from './deal-archived.event-handler';
import { LeadReopenedEventHandler } from './lead-reopened.event-handler';
import { DealReopenedEventHandler } from './deal-reopened.event-handler';
import { PipelineStageAddedEventHandler } from './pipeline-stage-added.event-handler';
import { PipelineStageRemovedEventHandler } from './pipeline-stage-removed.event-handler';
import { PipelineEditedEventHandler } from './pipeline-edited.event-handler';
import { UserAddedEventHandler } from './user-added.event-handler';
import { PipelineAddedEventHandler } from './pipeline-added.event-handler';

export const DealEventHandlers = [
  DealAddedEventHandler,
  DealDeletedEventHandler,
  DealEditedEventHandler,
  PipelineStageRenamedEventHandler,
  OrganizationRelatedToDealEventHandler,
  OrganizationUnrelatedFromDealEventHandler,
  PersonRelatedToDealEventHandler,
  PersonUnrelatedFromDealEventHandler,
  ContactPersonAssignedToDealEventHandler,
  ContactPersonRemovedFromDealEventHandler,
  DealWonEventHandler,
  DealLostEventHandler,
  DealStageChangedEventHandler,
  LeadCreatedFromDraftEventHandler,
  BudgetAllocatedForDealEventHandler,
  CustomerRequirementChangedForDealEventHandler,
  DecisionMakerAssignedToDealEventHandler,
  DecisionMakerRemovedFromDealEventHandler,
  TimingChangedForDealEventHandler,
  BudgetHolderAssignedToDealEventHandler,
  BudgetHolderRemovedFromDealEventHandler,
  LeadQualifiedEventHandler,
  LeadPromotedToDealEventHandler,
  DealDemotedToLeadEventHandler,
  PersonRelatedToLeadEventHandler,
  PersonUnrelatedFromLeadEventHandler,
  OrganizationRelatedToLeadEventHandler,
  OrganizationUnrelatedFromLeadEventHandler,
  TenantDeletedEventHandler,
  PipelineDeletedEventHandler,
  DealSortedEventHandler,
  LeadUnqualifiedEventHandler,
  LeadArchivedEventHandler,
  DealArchivedEventHandler,
  LeadReopenedEventHandler,
  DealReopenedEventHandler,
  PipelineStageAddedEventHandler,
  PipelineStageRemovedEventHandler,
  PipelineEditedEventHandler,
  PipelineAddedEventHandler,
  UserAddedEventHandler,
];
