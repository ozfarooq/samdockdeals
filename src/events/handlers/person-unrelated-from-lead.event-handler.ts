import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { PersonUnrelatedFromLeadEvent } from '@daypaio/domain-events/deals';

@EventsHandler(PersonUnrelatedFromLeadEvent)
export class PersonUnrelatedFromLeadEventHandler implements
    IEventHandler<PersonUnrelatedFromLeadEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: PersonUnrelatedFromLeadEvent) {
    this.logger.verbose('EVENT TRIGGERED');
    const { _leadID, _personID, meta } = event;
    try {
      await this.repository.unrelatePerson(_leadID, _personID, meta);
    } catch (error) {
      this.logger.error('Event failed!');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
