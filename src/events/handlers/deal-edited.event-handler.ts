import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DealEditedEvent, LeadEditedEvent } from '@daypaio/domain-events/deals';

@EventsHandler(DealEditedEvent, LeadEditedEvent)
export class DealEditedEventHandler
  implements IEventHandler<DealEditedEvent | LeadEditedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: DealRepository) {}
  async handle(event: DealEditedEvent | LeadEditedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _id, deal, meta } = event;
    try {
      await this.repository.edit(_id, deal, meta);
    } catch (error) {
      this.logger.error(`Failed to edit deal of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
