import { TestingModule, Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { DealEventHandlers } from '.';
import { DealRepository } from '../../repositories/deals.repository';
import { Deal } from '../../models/deal.model';
import { DealAddedEventHandler } from './deal-added.event-handler';
import { DealEditedEventHandler } from './deal-edited.event-handler';
import { DealDeletedEventHandler } from './deal-deleted.event-handler';
import { DealAddedEvent, DealEditedEvent, DealDeletedEvent } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { PipelineRepository } from '../../repositories/pipeline.repository';
import { DealsService } from '../../services/deals.service';
import { PipelineService } from '../../services/pipeline.service';

const mockDealRepository = () => ({
  add: jest.fn(),
  edit: jest.fn(),
  delete: jest.fn(),
  sortDeal: jest.fn(),
});

const mockPipelineRepository = () => ({
  sortDeal: jest.fn(),
  add: jest.fn(),
  read: jest.fn(),
});

const mockMeta = new EventMetaData('mocktenant', 'mock123');

function createMockDeals(): Deal[] {
  const firstDeal = new Deal({
    _id: 'org1',
    name: 'Alam',
    stage: {
      stageID: 'mockstageid',
      pipelineID: 'mocktenant',
    },
  });

  const secondDeal = new Deal({
    _id: 'org2',
    name: 'Grosch',
    stage: {
      stageID: 'mockstageid',
      pipelineID: 'mocktenant',
    },
  });

  const arrayOfDeals: Deal[] = [];
  arrayOfDeals.push(firstDeal);
  arrayOfDeals.push(secondDeal);

  return arrayOfDeals;
}

describe('DealEventHandlers', () => {
  const mockDeals: Deal[] = createMockDeals();
  let eventBus: EventBus;
  let repo;

  let addDealEventHandler: DealAddedEventHandler;
  let editDealEventHandler: DealEditedEventHandler;
  let deleteDealEventHandler: DealDeletedEventHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ...DealEventHandlers,
        DealsService,
        PipelineService,
        {
          provide: DealRepository,
          useFactory: mockDealRepository,
        },
        {
          provide: PipelineRepository,
          useFactory: mockPipelineRepository,
        },
      ],
    }).compile();
    eventBus = module.get<EventBus>(EventBus);
    repo = module.get<DealRepository>(DealRepository);

    addDealEventHandler = module.get<DealAddedEventHandler>(
      DealAddedEventHandler,
    );
    editDealEventHandler = module.get<DealEditedEventHandler>(
      DealEditedEventHandler,
    );
    deleteDealEventHandler = module.get<DealDeletedEventHandler>(
      DealDeletedEventHandler,
    );
  });

  describe(' for AddDealEventHandler', () => {
    it('should successfully return an array of deals', async () => {
      repo.add.mockResolvedValue(null);
      eventBus.register([DealAddedEventHandler]);
      expect(repo.add).not.toHaveBeenCalled();
      await eventBus.publish(
        new DealAddedEvent(
          mockDeals[0]._id,
          mockDeals[0],
          mockMeta,
        ),
      );
      expect(repo.add).toHaveBeenCalledWith(mockDeals[0], mockMeta);
    });
  });

  describe(' for EditDealEventHandler', () => {
    it('should successfully repo.updatedById() without any errors', async () => {
      repo.edit.mockResolvedValue(null);
      eventBus.register([DealEditedEventHandler]);
      expect(repo.edit).not.toHaveBeenCalled();
      await eventBus.publish(
        new DealEditedEvent(
          mockDeals[0]._id,
          mockDeals[0],
          mockMeta,
        ),
      );
      expect(repo.edit).toHaveBeenCalledWith(
        mockDeals[0]._id,
        mockDeals[0],
        mockMeta,
      );
    });
  });

  describe(' for DeleteDealEventHandler', () => {
    it('should successfully repo.delete() without any errors', async () => {
      repo.edit.mockResolvedValue(null);
      eventBus.register([DealDeletedEventHandler]);
      expect(repo.delete).not.toHaveBeenCalled();
      await eventBus.publish(
        new DealDeletedEvent(mockDeals[0]._id, mockMeta),
      );
      expect(repo.delete).toHaveBeenCalledWith(
        mockDeals[0]._id,
        mockMeta,
      );
    });

  });
});
