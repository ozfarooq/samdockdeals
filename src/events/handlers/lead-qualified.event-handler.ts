import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { LeadQualifiedEvent } from '@daypaio/domain-events/deals';
import { QUALIFIED_STAGE_ID } from '../../models/pipeline.model';

@EventsHandler(LeadQualifiedEvent)
export class LeadQualifiedEventHandler implements
    IEventHandler<LeadQualifiedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DealRepository) {}

  async handle(event: LeadQualifiedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _leadID, meta } = event;
    try {
      await this.repository.changeStage(
        _leadID,
        { stageID: QUALIFIED_STAGE_ID, pipelineID: null },
        meta,
      );
    } catch (error) {
      this.logger.error(error);
    }
  }
}
