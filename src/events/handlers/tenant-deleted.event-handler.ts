import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealsService } from '../../services/deals.service';
import { TenantDeletedEvent } from '@daypaio/domain-events/tenants';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { PipelineRepository } from '../../repositories/pipeline.repository';

@EventsHandler(TenantDeletedEvent)
export class TenantDeletedEventHandler
  implements IEventHandler<TenantDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    private dealService: DealsService,
    private pipelineRepository: PipelineRepository,
  ) { }
  async handle(event: TenantDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id } = event;
    try {
      const meta = new EventMetaData(_id, null);
      await this.dealService.deleteAll(meta);
      await this.pipelineRepository.deleteAll(meta);
    } catch (error) {
      this.logger.error(`Failed to delete deal of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
