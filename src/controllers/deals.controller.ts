import {
  Controller,
  Get,
  Logger,
  Patch,
  Param,
  Body,
  Post,
  Delete,
} from '@nestjs/common';
import { DealsService } from '../services/deals.service';
import { BreadController } from '../shared/controllers/bread.controller';
import {
  ApiTags,
  ApiOkResponse,
  ApiUnauthorizedResponse,  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiBearerAuth,
  ApiOperation,
} from '@nestjs/swagger';
import {
  GetUser,
  LoggedInUser,
} from '../shared/decorators/get-user.decorator';
import { Deal } from '../models/deal.model';
import { DealDTO } from '../dto/deal.dto';
import { IDeal, IDealStage, IDealBudget } from '@daypaio/domain-events/deals';

@ApiTags('deals')
@ApiBearerAuth()
@Controller('')
export class DealController implements BreadController<IDeal> {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: DealsService) {}

  @Get()
  @ApiOperation({
    summary: 'Browse Deals',
    description: 'Get all deals',
  })
  @ApiOkResponse({
    description: 'The records have successfully been queried',
    type: Deal,
    isArray: true,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browse(@GetUser() user: LoggedInUser): Promise<IDeal[]> {
    this.logger.log(`BROWSE DEALS user:${user._id}`);
    return await this.service.browse(user.generateMeta());
  }

  @Get('/:id')
  @ApiOperation({
    summary: 'Read Deal',
    description: 'Get an deal by id',
  })
  @ApiOkResponse({
    description: 'The deal has successfully been found',
    type: Deal,
  })
  @ApiNotFoundResponse({ description: 'The deal was not found' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async read(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<IDeal> {
    this.logger.log(`READ id:${id} user:${user._id}`);
    return await this.service.read(id, user.generateMeta());
  }

  @Patch('/:id')
  @ApiOperation({
    summary: 'Edit Deal',
    description: 'Edit an deal',
  })
  @ApiOkResponse({
    description:
      'The deal editing event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async edit(
    @Param('id') id: string,
    @Body() deal: DealDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    await this.service.edit(id, deal, user.generateMeta());
  }

  @Patch('/:id/stage')
  @ApiOperation({
    summary: 'Change Deal Stage',
    description: 'Change the stage of a deal',
  })
  @ApiOkResponse({
    description:
      'The deal stage was successfully changed',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeStage(
    @Param('id') id: string,
    @Body() stage: IDealStage,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    await this.service.changeStage(id, stage, user.generateMeta());
  }

  @Patch('/:id/stage/demote')
  @ApiOperation({
    summary: 'Demote a deal to a lead',
    description: 'Demote a deal to a lead',
  })
  @ApiOkResponse({
    description:
      'The deal stage was successfully demoted',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async demoteDeal(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    await this.service.demote(id, user.generateMeta());
  }

  @Patch('/:id/archive')
  @ApiOperation({
    summary: 'Archive a deal',
    description: 'Archive a deal',
  })
  @ApiOkResponse({
    description:
      'The deal was successfully archived',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async archiveDeal(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`ARCHIVE id:${id} user:${user._id}`);
    await this.service.archive(id, user.generateMeta());
  }

  @Patch('/:id/reopen')
  @ApiOperation({
    summary: 'Reopen a deal',
    description: 'Reopen a deal that is archived',
  })
  @ApiOkResponse({
    description:
      'The deal stage was successfully reopened',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async reopenDeal(
    @Param('id') id: string,
    @Body('pipelineID') pipelineID: string,
    @Body('stageID') stageID: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`REOPEN id:${id} user:${user._id}`);
    await this.service.reopen(id, pipelineID, stageID, user.generateMeta());
  }

  @Patch('/:id/budget')
  @ApiOperation({
    summary: 'Change the budget of the deal/lead',
    description: 'Change the budgett of a deal',
  })
  @ApiOkResponse({
    description:
      'The deal budget was successfully changed',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeBudget(
    @Param('id') id: string,
    @Body() budget: IDealBudget,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE BUDGET id:${id} user:${user._id}`);
    await this.service.changeBudget(
      id,
      Object.keys(budget).length !== 0 ? budget : null ,
      user.generateMeta(),
    );
  }

  @Patch('/:id/decisionMaker')
  @ApiOperation({
    summary: 'Change the decision maker',
    description: 'Change the decision  maker',
  })
  @ApiOkResponse({
    description:
      'The decision maker of the budget was successfully changed',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeDecisionMaker(
    @Param('id') id: string,
    @Body('decisionMakerID') decisionMakerID: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE DECISION MAKER id:${id} user:${user._id}`);
    await this.service.changeDecisionMaker(id, decisionMakerID, user.generateMeta());
  }

  @Patch('/:id/budgetHolder')
  @ApiOperation({
    summary: 'Change the budget holder',
    description: 'Change the budget holder',
  })
  @ApiOkResponse({
    description:
      'The budget holder of the budghet was successfully changed',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeBudgetHolder(
    @Param('id') id: string,
    @Body('budgetHolderID') budgetHolderID: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE BUDGET HOLDER id:${id} user:${user._id}`);
    await this.service.changeBudgetHolder(id, budgetHolderID, user.generateMeta());
  }

  @Patch('/:id/customerRequirement')
  @ApiOperation({
    summary: 'Change the customer requirement',
    description: 'Change the customer requirement',
  })
  @ApiOkResponse({
    description:
      'Change the customer requirement of the deal',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeCustomerRequirement(
    @Param('id') id: string,
    @Body('customerRequirement') customerRequirement: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE DECISION MAKER id:${id} user:${user._id}`);
    await this.service.changeCustomerRequirement(id, customerRequirement, user.generateMeta());
  }

  @Patch('/:id/timing')
  @ApiOperation({
    summary: 'Change the timing of the deal',
    description: 'Change the timing of the deal',
  })
  @ApiOkResponse({
    description:
      'Change the timing of the deal',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeTiming(
    @Param('id') id: string,
    @Body('timing') timing: number,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE TIMING id:${id} user:${user._id}`);
    await this.service.changeTiming(id, timing, user.generateMeta());
  }

  @Post()
  @ApiOperation({
    summary: 'Add Deal',
    description: 'Create an deal',
  })
  @ApiOkResponse({
    description:
      'The deal adding event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(
    @Body() deal: DealDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CREATE id:${deal._id} user:${user._id}`);
    await this.service.add(deal, user.generateMeta());
  }

  @Delete('/:id')
  @ApiOperation({
    summary: 'Delete Deal',
    description: 'Delete an deal',
  })
  @ApiOkResponse({
    description:
      'The deal deleting event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`DELETE id:${id} user:${user._id}`);
    await this.service.delete(id, user.generateMeta());
  }
}
