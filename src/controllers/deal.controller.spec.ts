import { Test, TestingModule } from '@nestjs/testing';
import { DealController } from './deals.controller';
import { DealsService } from '../services/deals.service';

const mockDealService = () => ({});

describe('Deal Controller', () => {
  let controller: DealController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DealController],
      providers: [
        { provide: DealsService, useValue: mockDealService },
      ],
    }).compile();

    controller = module.get<DealController>(DealController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
