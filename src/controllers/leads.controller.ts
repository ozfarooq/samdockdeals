import { ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import { Controller, Logger, Get, Patch, Param, Body, Delete, Post } from '@nestjs/common';
import { Deal } from '../models/deal.model';
import { GetUser, LoggedInUser } from '../shared/decorators/get-user.decorator';
import { IDeal, IDealBudget } from '@daypaio/domain-events/deals';
import { DealDTO } from '../dto/deal.dto';
import { LeadsService } from '../services/leads.service';

@ApiTags('leads')
@ApiBearerAuth()
@Controller('leads')
export class LeadController {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: LeadsService) { }

  @Get()
  @ApiOperation({
    summary: 'Browse Leads',
    description: 'Get all leads',
  })
  @ApiOkResponse({
    description: 'The records have successfully been queried',
    type: Deal,
    isArray: true,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browse(@GetUser() user: LoggedInUser): Promise<IDeal[]> {
    this.logger.log(`BROWSE LEADS user:${user._id}`);
    return await this.service.browse(user.generateMeta());
  }

  @Get('combined')
  @ApiOperation({
    summary: 'Browse Leads and Deals',
    description: 'Get all leads and deals',
  })
  @ApiOkResponse({
    description: 'The records have successfully been queried',
    type: Deal,
    isArray: true,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browseAll(@GetUser() user: LoggedInUser): Promise<IDeal[]> {
    this.logger.log(`BROWSE LEADS user:${user._id}`);
    return await this.service.browseAll(user.generateMeta());
  }

  @Patch('/:id')
  @ApiOperation({
    summary: 'Edit Lead',
    description: 'Edit an Lead',
  })
  @ApiOkResponse({
    description:
      'The Lead editing event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async edit(
    @Param('id') id: string,
    @Body() lead: DealDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    await this.service.edit(id, lead, user.generateMeta());
  }

  @Patch('/:id/stage/qualify')
  @ApiOperation({
    summary: 'Qualify a lead',
    description: 'Qualify a lead',
  })
  @ApiOkResponse({
    description:
      'The lead stage was successfully qualified',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async qualifyLead(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    await this.service.qualify(id, user.generateMeta());
  }

  @Patch('/:id/stage/unqualify')
  @ApiOperation({
    summary: 'Demote a deal to a lead',
    description: 'Demote a deal to a lead',
  })
  @ApiOkResponse({
    description:
      'The Lead stage was successfully demoted',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async unqualifyLead(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    await this.service.unqualify(id, user.generateMeta());
  }

  @Patch('/:id/stage/promote')
  @ApiOperation({
    summary: 'Promote a lead to a deal',
    description: 'Qualify a lead',
  })
  @ApiOkResponse({
    description:
      'The lead was successfully promoted to a deal',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async promoteLead(
    @Param('id') id: string,
    @Body('pipelineID') pipelineID: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    await this.service.promote(id, pipelineID, user.generateMeta());
  }

  @Patch('/:id/budget')
  @ApiOperation({
    summary: 'Change the budget of the Lead/lead',
    description: 'Change the budgett of a Lead',
  })
  @ApiOkResponse({
    description:
      'The Lead budget was successfully changed',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeBudget(
    @Param('id') id: string,
    @Body() budget: IDealBudget,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE BUDGET id:${id} user:${user._id}`);
    await this.service.changeBudget(
      id,
      Object.keys(budget).length !== 0 ? budget : null ,
      user.generateMeta(),
    );
  }

  @Patch('/:id/decisionMaker')
  @ApiOperation({
    summary: 'Change the decision maker',
    description: 'Change the decision  maker',
  })
  @ApiOkResponse({
    description:
      'The decision maker of the budget was successfully changed',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeDecisionMaker(
    @Param('id') id: string,
    @Body('decisionMakerID') decisionMakerID: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE DECISION MAKER id:${id} user:${user._id}`);
    await this.service.changeDecisionMaker(id, decisionMakerID, user.generateMeta());
  }

  @Patch('/:id/budgetHolder')
  @ApiOperation({
    summary: 'Change the budget holder',
    description: 'Change the budget holder',
  })
  @ApiOkResponse({
    description:
      'The budget holder of the budget was successfully changed',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeBudgetHolder(
    @Param('id') id: string,
    @Body('budgetHolderID') budgetHolderID: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE BUDGET HOLDER id:${id} user:${user._id}`);
    await this.service.changeBudgetHolder(id, budgetHolderID, user.generateMeta());
  }

  @Patch('/:id/customerRequirement')
  @ApiOperation({
    summary: 'Change the customer requirement',
    description: 'Change the customer requirement',
  })
  @ApiOkResponse({
    description:
      'Change the customer requirement of the Lead',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeCustomerRequirement(
    @Param('id') id: string,
    @Body('customerRequirement') customerRequirement: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE DECISION MAKER id:${id} user:${user._id}`);
    await this.service.changeCustomerRequirement(id, customerRequirement, user.generateMeta());
  }

  @Patch('/:id/timing')
  @ApiOperation({
    summary: 'Change the timing of the Lead',
    description: 'Change the timing of the Lead',
  })
  @ApiOkResponse({
    description:
      'Change the timing of the Lead',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeTiming(
    @Param('id') id: string,
    @Body('timing') timing: number,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CHANGE TIMING id:${id} user:${user._id}`);
    await this.service.changeTiming(id, timing, user.generateMeta());
  }

  @Post()
  @ApiOperation({
    summary: 'Add Lead',
    description: 'Create an Lead',
  })
  @ApiOkResponse({
    description:
      'The Lead adding event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(
    @Body() lead: DealDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CREATE id:${lead._id} user:${user._id}`);
    await this.service.add(lead, user.generateMeta());
  }

  @Delete('/:id')
  @ApiOperation({
    summary: 'Delete Lead',
    description: 'Delete an Lead',
  })
  @ApiOkResponse({
    description:
      'The Lead deleting event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`DELETE id:${id} user:${user._id}`);
    await this.service.delete(id, user.generateMeta());
  }

  @Patch('/:id/archive')
  @ApiOperation({
    summary: 'Archive a lead',
    description: 'Archive a lead',
  })
  @ApiOkResponse({
    description:
      'The lead was successfully archived',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async archiveLead(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`ARCHIVE id:${id} user:${user._id}`);
    await this.service.archive(id, user.generateMeta());
  }

  @Patch('/:id/reopen')
  @ApiOperation({
    summary: 'Reopen a lead',
    description: 'Reopen a lead that is archived',
  })
  @ApiOkResponse({
    description:
      'The lead was successfully reopened',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async reopenLead(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`REOPEN id:${id} user:${user._id}`);
    await this.service.reopen(id, user.generateMeta());
  }

}
