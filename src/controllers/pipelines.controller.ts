import {
  Controller,
  Get,
  Logger,
  Patch,
  Param,
  Body,
  Post,
  Delete,
  Put,
  UseGuards,
  BadRequestException,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import {
  GetUser,
  LoggedInUser,
} from '../shared/decorators/get-user.decorator';
import { PipelineService } from '../services/pipeline.service';
import { Pipeline } from '../models/pipeline.model';
import { RemovePipelineStageDTO } from '../dto/remove-pipeline-stage.dto';
import { IPipeline, IPipelineStage } from '@daypaio/domain-events/deals';
import { AdminGuard } from '../shared/guards/admin.guard';
import { SkipAdminGuard } from '../shared/decorators/skip-admin-guard.decorator';

@ApiTags('pipelines')
@ApiBearerAuth()
@UseGuards(AdminGuard)
@Controller('pipelines')
export class PipelinesController {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: PipelineService) {}

  @SkipAdminGuard()
  @Get()
  @ApiOperation({
    summary: 'Browse Pipelines',
    description: 'Browse all deal pipelines',
  })
  @ApiOkResponse({
    description: 'Returning the pipelines',
    type: Pipeline,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browse(@GetUser() user: LoggedInUser): Promise<Pipeline[]> {
    return this.service.browse(user.generateMeta());
  }

  @Post()
  @ApiOperation({
    summary: 'Add Pipeline',
    description: 'Add a deal pipeline',
  })
  @ApiOkResponse({
    description: 'Added a pipeline',
    type: Pipeline,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(@GetUser() user: LoggedInUser, @Body() pipeline: IPipeline): Promise<void> {
    return this.service.add(pipeline, user.generateMeta());
  }

  @Patch(':pipelineID')
  @ApiOperation({
    summary: 'Edit Pipeline',
    description: 'Add a deal pipeline',
  })
  @ApiOkResponse({
    description: 'Added a pipeline',
    type: Pipeline,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async edit(
    @Param('pipelineID') pipelineID,
    @Body() pipeline: Partial<IPipeline>,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    return this.service.edit(pipelineID, pipeline, user.generateMeta());
  }

  @Patch(':pipelineID/stages/:id')
  @ApiOperation({
    summary: 'Change name of PipelineStage',
    description: 'Change name of PipelineStage',
  })
  @ApiOkResponse({
    description:
      'The pipeline stage has been renamed',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async renamePipelineStage(
    @Param('id') id: string,
    @Param('pipelineID') pipelineID: string,
    @Body('name') newName: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    await this.service.renamePipelineStage(pipelineID, id, newName, user.generateMeta());
  }

  @Put(':pipelineID/stages/:id/:newPosition')
  @ApiOperation({
    summary: 'Change stage position',
    description: 'Change stage position',
  })
  @ApiOkResponse({
    description:
      'The stage has been moved',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async changeStagePosition(
    @Param('id') id: string,
    @Param('pipelineID') pipelineID: string,
    @Param('newPosition') newPosition: number,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    await this.service.changeStagePosition(pipelineID, id, newPosition, user.generateMeta());
  }

  @Post(':pipelineID/stages')
  @ApiOperation({
    summary: 'Add pipeline stage',
    description: 'Create an pipeline stage',
  })
  @ApiOkResponse({
    description:
      'The pipeline stage adding event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async addPipelineStage(
    @Param('pipelineID') pipelineID: string,
    @Body() stage: IPipelineStage,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CREATE stage id:${stage._id} user:${user._id}`);
    await this.service.addStage(pipelineID, stage, user.generateMeta());
  }

  @Delete(':pipelineID/stages/:id')
  @ApiOperation({
    summary: 'Remove pipeline stage',
    description: 'Remove a pipeline stage',
  })
  @ApiOkResponse({
    description:
      'The pipeline stage removing event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async removePipelineStage(
    @Param('pipelineID') pipelineID: string,
    @Param('id') id: string,
    @Body() removePipelineStageDTO: RemovePipelineStageDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    const moveDealsTo = removePipelineStageDTO?.moveDealsTo || null;
    this.logger.log(`REMOVE stage id:${id} and move deals to id:${moveDealsTo} user:${user._id}`);
    const meta = user.generateMeta();
    await this.service.removeStage(pipelineID, id, moveDealsTo, meta);
  }

  @Delete(':pipelineID')
  @ApiOperation({
    summary: 'Remove pipeline and all related deals',
    description: 'Remove pipeline and all related deals',
  })
  @ApiOkResponse({
    description:
      'The pipeline removing event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(
    @Param('pipelineID') pipelineID: string,
    @Body('toPipelineID') toPipelineID: string,
    @Body('toStageID') toStageID: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    const meta = user.generateMeta();
    if (pipelineID === meta._tenantID) {
      throw new BadRequestException();
    }
    this.logger.log(`REMOVE deal pipeline id:${meta._tenantID}`);
    await this.service.delete(pipelineID, toPipelineID || null, toStageID || null, meta);
  }

  @Patch(':pipelineID/stages/:id/sort')
  @ApiOperation({
    summary: 'Sort deal inside the stage',
    description: 'Sort deal inside the stage',
  })
  @ApiOkResponse({
    description:
      'The stage has been resorted',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async sortDeal(
    @Param('pipelineID') pipelineID: string,
    @Param('id') stageID: string,
    @Body('dealID') dealID: string,
    @Body('sortIndex') sortIndex: number,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    await this.service.sortDeal(pipelineID, stageID, dealID, sortIndex, user.generateMeta());
  }

}
