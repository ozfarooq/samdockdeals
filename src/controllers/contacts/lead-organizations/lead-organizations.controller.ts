import { Controller, Logger, Post, Param, Body, Delete } from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiBody,
} from '@nestjs/swagger';
import { GetUser, LoggedInUser } from '../../../shared/decorators/get-user.decorator';
import { LeadsService } from '../../../services/leads.service';

class AddOrganizationToLeadDTO {
  _id: string;
}

@ApiTags('leads')
@ApiBearerAuth()
@Controller('leads/:leadID/organizations')
export class LeadOrganizationsController {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: LeadsService) { }

  @Post('')
  @ApiOperation({
    summary: 'Relate Organization To Lead',
    description: 'Relate a organization to a lead, add the body with { "_id": "some_id"}',
  })
  @ApiOkResponse({
    description: 'The organization was successfully related to a deal',
  })
  @ApiBody({ type: AddOrganizationToLeadDTO })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  relatePersonToLead(
    @Param('leadID') leadID: string,
    @Body('_id') organizationID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.relateOrganization(leadID, organizationID, user.generateMeta());
  }

  @Delete(':organizationID')
  @ApiOperation({
    summary: 'Unrelate Organization To Lead',
    description: 'Unrelate a organization to a lead',
  })
  @ApiOkResponse({
    description: 'The organization was successfully unrelated to a deal',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  unrelatePersonToLead(
    @Param('leadID') leadID: string,
    @Param('organizationID') organizationID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.unrelateOrganization(leadID, organizationID, user.generateMeta());
  }
}
