import { Test, TestingModule } from '@nestjs/testing';
import { LeadOrganizationsController } from './lead-organizations.controller';
import { LeadsService } from '../../../services/leads.service';

describe('LeadOrganizations Controller', () => {
  let controller: LeadOrganizationsController;

  const mockDealService = () => ({});
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LeadOrganizationsController],
      providers: [
        { provide: LeadsService, useValue: mockDealService },
      ],
    }).compile();

    controller = module.get<LeadOrganizationsController>(LeadOrganizationsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
