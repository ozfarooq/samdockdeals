import { Controller, Logger, Post, Param, Body, Delete } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { DealsService } from '../../../services/deals.service';
import {
  GetUser,
  LoggedInUser,
} from '../../../shared/decorators/get-user.decorator';

@ApiTags('deals organizations')
@ApiBearerAuth()
@Controller(':dealID/organizations/')
export class DealOrganizationsController {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: DealsService) {}

  @Post('')
  relateOrganizationToDeal(
    @Param('dealID') dealID: string,
    @Body('_id') organizationID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.relateOrganization(
      dealID,
      organizationID,
      user.generateMeta(),
    );
  }

  @Delete('/:organizationID')
  unrelateOrganizationToDeal(
    @Param('dealID') dealID: string,
    @Param('organizationID') organizationID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.unrelateOrganization(
      dealID,
      organizationID,
      user.generateMeta(),
    );
  }
}
