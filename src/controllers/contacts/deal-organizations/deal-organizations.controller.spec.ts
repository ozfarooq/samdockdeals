import { Test, TestingModule } from '@nestjs/testing';
import { DealOrganizationsController } from './deal-organizations.controller';
import { DealsService } from '../../../services/deals.service';

describe('DealOrganizations Controller', () => {
  let controller: DealOrganizationsController;

  const mockDealService = () => ({});
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DealOrganizationsController],
      providers: [
        { provide: DealsService, useValue: mockDealService },
      ],
    }).compile();

    controller = module.get<DealOrganizationsController>(DealOrganizationsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
