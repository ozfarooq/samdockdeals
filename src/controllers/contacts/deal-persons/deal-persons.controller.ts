import { Controller, Logger, Post, Param, Body, Delete, Patch } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { DealsService } from '../../../services/deals.service';
import {
  GetUser,
  LoggedInUser,
} from '../../../shared/decorators/get-user.decorator';

@ApiTags('deals persons')
@ApiBearerAuth()
@Controller(':dealID/persons')
export class DealPersonsController {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: DealsService) {}
  @Post('')
  relatePersonToDeal(
    @Param('dealID') dealID: string,
    @Body('_id') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.relatePerson(dealID, personID, user.generateMeta());
  }

  @Delete('/:personID')
  unrelatePersonToDeal(
    @Param('dealID') dealID: string,
    @Param('personID') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.unrelatePerson(dealID, personID, user.generateMeta());
  }

  @Patch(':personID/assignAsContactPerson')
  assignContactPerson(
    @Param('dealID') dealID: string,
    @Param('personID') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.assignContactPerson(
      dealID,
      personID,
      user.generateMeta(),
    );
  }

  @Delete(':personID/removeAsContactPerson')
  removeContactPerson(
    @Param('dealID') dealID: string,
    @Param('personID') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.removeContactPerson(
      dealID,
      personID,
      user.generateMeta(),
    );
  }
}
