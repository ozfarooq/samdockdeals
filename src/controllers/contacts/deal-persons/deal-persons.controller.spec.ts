import { Test, TestingModule } from '@nestjs/testing';
import { DealPersonsController } from './deal-persons.controller';
import { DealsService } from '../../../services/deals.service';

describe('DealPersons Controller', () => {
  let controller: DealPersonsController;

  const mockDealService = () => ({});
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DealPersonsController],
      providers: [
        { provide: DealsService, useValue: mockDealService },
      ],
    }).compile();

    controller = module.get<DealPersonsController>(DealPersonsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
