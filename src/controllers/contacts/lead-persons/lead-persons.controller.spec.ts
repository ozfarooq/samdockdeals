import { Test, TestingModule } from '@nestjs/testing';
import { LeadPersonsController } from './lead-persons.controller';
import { LeadsService } from '../../../services/leads.service';

describe('LeadPersons Controller', () => {
  let controller: LeadPersonsController;

  const mockDealService = () => ({});
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LeadPersonsController],
      providers: [
        { provide: LeadsService, useValue: mockDealService },
      ],
    }).compile();

    controller = module.get<LeadPersonsController>(LeadPersonsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
