import { Controller, Logger, Post, Param, Body, Delete, Patch } from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiBody,
} from '@nestjs/swagger';
import { GetUser, LoggedInUser } from '../../../shared/decorators/get-user.decorator';
import { LeadsService } from '../../../services/leads.service';

class AddPersonToLeadDTO {
  _id: string;
}

@ApiTags('leads')
@ApiBearerAuth()
@Controller('leads/:leadID/persons')
export class LeadPersonsController {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: LeadsService) { }
  @Post('')
  @ApiOperation({
    summary: 'Relate Person To Lead',
    description: 'Relate a person to a lead, add the body with { "_id": "some_id"}',
  })
  @ApiOkResponse({
    description: 'The person was successfully related to a lead',
  })
  @ApiBody({ type: AddPersonToLeadDTO })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  relatePersonToLead(
    @Param('leadID') leadID: string,
    @Body('_id') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.relatePerson(leadID, personID, user.generateMeta());
  }

  @Delete(':personID')
  @ApiOperation({
    summary: 'Unrelate Person from Lead',
    description: 'Unrelate a person from a lead',
  })
  @ApiOkResponse({
    description: 'The person was successfully unrelated from a lead',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  unrelatePersonToLead(
    @Param('leadID') leadID: string,
    @Param('personID') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.unrelatePerson(leadID, personID, user.generateMeta());
  }

  @Patch(':personID/assignAsContactPerson')
  assignContactPerson(
    @Param('leadID') leadID: string,
    @Param('personID') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.assignContactPerson(
      leadID,
      personID,
      user.generateMeta(),
    );
  }

  @Delete(':personID/removeAsContactPerson')
  removeContactPerson(
    @Param('leadID') leadID: string,
    @Param('personID') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.removeContactPerson(
      leadID,
      personID,
      user.generateMeta(),
    );
  }
}
