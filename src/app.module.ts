import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { HealthcheckModule } from './shared/healthcheck/healthcheck.module';
import { ConfigModule, ConfigService } from 'nestjs-config';
import * as path from 'path';
import { EventStoreCqrsModule } from 'nestjs-eventstore';
import { eventStoreBusConfig } from './event-bus.provider';
import { DealController } from './controllers/deals.controller';
import { DealsService } from './services/deals.service';
import { DealRepository } from './repositories/deals.repository';
import { DealQueryHandlers } from './queries/handlers';
import { DealCommandHandlers } from './commands/handlers';
import { DealEventHandlers } from './events/handlers';
import { Deal } from './models/deal.model';
import { Pipeline } from './models/pipeline.model';
import { PipelineService } from './services/pipeline.service';
import { PipelineRepository } from './repositories/pipeline.repository';
import { PipelinesController } from './controllers/pipelines.controller';
import { DealPersonsController } from './controllers/contacts/deal-persons/deal-persons.controller';
import { DealOrganizationsController } from './controllers/contacts/deal-organizations/deal-organizations.controller';
import { LeadController } from './controllers/leads.controller';
import { DealCreatedFromDraftSaga } from './sagas/lead-created-from-draft.saga';
import { LeadStageChangedSaga } from './sagas/lead-stage-changed.saga';
import { ArchiveLostDealsSaga } from './sagas/archive-lost-deals.saga';
import { LeadsService } from './services/leads.service';
import { LeadPersonsController } from './controllers/contacts/lead-persons/lead-persons.controller';
import { LeadOrganizationsController } from './controllers/contacts/lead-organizations/lead-organizations.controller';

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
    TypegooseModule.forRootAsync({
      useFactory: async (config: ConfigService) => ({
        uri: config.get('database.mongoConnectionString'),
      }),
      inject: [ConfigService],
    }),
    TypegooseModule.forFeature([Deal, Pipeline]),
    EventStoreCqrsModule.forRootAsync(
      {
        useFactory: async (config: ConfigService) => {
          return {
            connectionSettings: config.get('eventstore.connectionSettings'),
            endpoint: config.get('eventstore.tcpEndpoint'),
          };
        },
        inject: [ConfigService],
      },
      eventStoreBusConfig,
    ),
    HealthcheckModule,
  ],
  controllers: [
    PipelinesController,
    LeadController,
    DealController,
    DealPersonsController,
    DealOrganizationsController,
    LeadPersonsController,
    LeadOrganizationsController,
  ],
  providers: [
    DealsService,
    LeadsService,
    DealRepository,
    PipelineService,
    PipelineRepository,
    DealCreatedFromDraftSaga,
    LeadStageChangedSaga,
    ArchiveLostDealsSaga,
    ...DealQueryHandlers,
    ...DealCommandHandlers,
    ...DealEventHandlers,
  ],
})
export class AppModule {}
