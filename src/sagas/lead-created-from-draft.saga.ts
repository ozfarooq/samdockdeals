import { Saga, ICommand } from '@nestjs/cqrs';
import { Observable, Subject } from 'rxjs';
import { DraftVerifiedEvent } from '@daypaio/domain-events/contact-drafts';
import { CreateLeadFromDraftCommand } from '../commands/impl/create-lead-from-draft.command';
import { AssignContactPersonToDealCommand } from '../commands/impl/assign-contact-person-to-deal.command';
import { ChangeDealStageCommand } from '../commands/impl/change-deal-stage.command';
import { filter } from 'rxjs/operators';
import { EditDealCommand } from '../commands/impl/edit-deal.command';
import { IUser } from '@daypaio/domain-events/users';
import { RelatePersonToLeadCommand } from '../commands/impl/relate-person-to-lead.command';
import { RelateOrganizationToLeadCommand } from '../commands/impl/relate-organization-to-lead.command';
import { PROSPECTING_STAGE_ID } from '../models/pipeline.model';

export class DealCreatedFromDraftSaga {
  @Saga()
    leadCreatedFromDraft = (events$: Observable<any>): Observable<ICommand> => {
      const subject: Subject<ICommand> = new Subject();
      events$.pipe(
        filter(event =>
          event instanceof DraftVerifiedEvent && event?.data?.createLead,
        ),
      ).subscribe((event: DraftVerifiedEvent) => {
        const { _id, data, meta } = event;
        subject.next(new CreateLeadFromDraftCommand(
          _id, null, null, meta,
        ));
        if (!!data.lastName) {
          subject.next(new RelatePersonToLeadCommand(_id, _id, meta));
          subject.next(new AssignContactPersonToDealCommand(_id, _id, meta));
        }
        if (!!data.companyName) {
          subject.next(new RelateOrganizationToLeadCommand(_id, _id, meta));
        }
        if (!!data.editor) {
          subject.next(new EditDealCommand(_id, { editor: { _id : data.editor } as IUser }, meta));
        }
        if (!!data.leadRequirements) {
          subject.next(
            new EditDealCommand(_id, { customerRequirement: data.leadRequirements }, meta),
          );
        }
        subject.next(
          new ChangeDealStageCommand(
            _id,
            { pipelineID: null, stageID: PROSPECTING_STAGE_ID },
            meta,
          ),
        );
      });

      return subject.asObservable();
    }
}
