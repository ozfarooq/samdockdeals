import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { ArchiveDealCommand } from '../commands/impl/archive-deal.command';
import { DealLostEvent } from '@daypaio/domain-events/deals';
import { map } from 'rxjs/operators';

export class ArchiveLostDealsSaga {
  @Saga()
  archiveLostDeals$ = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DealLostEvent),
      map(
        (event: DealLostEvent) => {
          return new ArchiveDealCommand(event._dealID, event.meta);
        },
      ),
    );
  }
}
