import { Saga, ICommand } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ChangeLeadQualificationCriteriaCommand } from '../commands/impl/change-lead-qualification-criteria.command';
import {
  BudgetAllocatedForDealEvent,
  DecisionMakerAssignedToDealEvent,
  BudgetHolderAssignedToDealEvent,
  CustomerRequirementChangedForDealEvent,
  TimingChangedForDealEvent,
} from '@daypaio/domain-events/deals';

const changeEvents = [
  BudgetAllocatedForDealEvent,
  DecisionMakerAssignedToDealEvent,
  BudgetHolderAssignedToDealEvent,
  CustomerRequirementChangedForDealEvent,
  TimingChangedForDealEvent,
];

type ChangeEvent = BudgetAllocatedForDealEvent |
  DecisionMakerAssignedToDealEvent |
  BudgetHolderAssignedToDealEvent |
  CustomerRequirementChangedForDealEvent |
  TimingChangedForDealEvent;

export class LeadStageChangedSaga {
  @Saga()
  leadStageChanged = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      filter((event) => {
        return changeEvents.some(eventType => event instanceof eventType);
      }),
      map(
        (event: ChangeEvent) => {
          return new ChangeLeadQualificationCriteriaCommand(event._dealID, event.meta);
        },
      ),
    );
  }
}
