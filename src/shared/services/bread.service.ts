import { CommandBus, QueryBus, ICommand, IQuery } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventMetaData } from '@daypaio/domain-events/shared';

export abstract class BreadService<T> {
  protected logger: Logger = new Logger(this.constructor.name);
  constructor(private commandBus: CommandBus, private queryBus: QueryBus) {}

  protected async executeCommand(command: ICommand): Promise<void> {
    if (this.commandBus) {
      this.logger.log(`${command.constructor.name} executed`);
      return await this.commandBus.execute(command);
    }
    this.logger.error(
      `Could not execute command of type ${command.constructor.name} on the command bus of wrong type
      `,
    );
  }

  protected async executeQuery(query: IQuery): Promise<any> {
    if (this.queryBus) {
      this.logger.log(`${query.constructor.name} executed`);
      return await this.queryBus.execute(query);
    }
    this.logger.error(
      `Could not execute query of type ${query.constructor.name} on the query bus of wrong type
      `,
    );
  }

  abstract browse(meta: EventMetaData): Promise<T[]>;

  abstract read(id: string, meta: EventMetaData): Promise<T>;

  abstract edit(id: string, object: T, meta: EventMetaData): Promise<void>;

  abstract add(object: T, meta: EventMetaData): Promise<void>;

  abstract delete(id: string, meta: EventMetaData): Promise<void>;
}
