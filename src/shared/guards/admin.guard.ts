import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { getUserFromJwt } from '../utils/getUserFromJwt';
import { Reflector } from '@nestjs/core';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  logger: Logger = new Logger(this.constructor.name);
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    try {
      const flag = this.reflector.get<boolean>(
        'skipAdminGuard',
        context.getHandler(),
      );
      if (flag) return true;
      const user = getUserFromJwt(
        context.getArgByIndex(0).headers.authorization,
      );
      return user.isAdmin;
    } catch (error) {
      this.logger.log('Could not decode user from jwt');
      throw new UnauthorizedException();
    }
  }
}
