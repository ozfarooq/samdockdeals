import { Injectable } from '@nestjs/common';
import { HealthCheckError } from '@godaddy/terminus';
import { HealthIndicatorResult, HealthIndicator } from '@nestjs/terminus';
import { EventStore } from 'nestjs-eventstore';

@Injectable()
export class EventStoreHealthIndicator extends HealthIndicator {
  constructor(private eventStore: EventStore) {
    super();
  }

  async isHealthy(key: string): Promise<HealthIndicatorResult> {
    const isHealthy = this.eventStore.isConnected;

    const result = this.getStatus(key, isHealthy);

    if (isHealthy) {
      return result;
    }
    throw new HealthCheckError('EventStore Healthcheck failed', result);
  }
}
