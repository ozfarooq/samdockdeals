import { Injectable } from '@nestjs/common';
import { Connection } from 'mongoose';
import {
  HealthIndicator,
  HealthIndicatorResult,
  ConnectionNotFoundError,
  TimeoutError,
} from '@nestjs/terminus';
import { promiseTimeout } from '@nestjs/terminus/dist/utils';
import { HealthCheckError } from '@godaddy/terminus';
import { ModuleRef } from '@nestjs/core';
import { getConnectionToken } from 'nestjs-typegoose';

export interface TypegoosePingCheckSettings {
  connectionName?: string;
  timeout?: number;
}

@Injectable()
export class TyepgooseHealthIndicator extends HealthIndicator {
  constructor(private moduleRef: ModuleRef) {
    super();
  }

  async pingDb(connection: Connection, timeout: number) {
    return await promiseTimeout(timeout, connection.startSession());
  }

  async pingCheck(
    key: string,
    options: TypegoosePingCheckSettings = {},
  ): Promise<HealthIndicatorResult> {
    let isHealthy = false;
    const connection = this.moduleRef.get(
      getConnectionToken(options.connectionName),
      { strict: false },
    );
    const timeout = options.timeout || 1000;

    if (!connection) {
      throw new ConnectionNotFoundError(
        this.getStatus(key, isHealthy, {
          message: 'Connection provider not found in application context',
        }),
      );
    }

    try {
      await this.pingDb(connection, timeout);
      isHealthy = true;
    } catch (err) {
      if (err instanceof TimeoutError) {
        throw new TimeoutError(
          timeout,
          this.getStatus(key, isHealthy, {
            message: `timeout of ${timeout}ms exceeded`,
          }),
        );
      }
    }

    if (isHealthy) {
      return this.getStatus(key, isHealthy);
    }
    throw new HealthCheckError(
      `${key} is not available`,
      this.getStatus(key, isHealthy),
    );
  }
}
