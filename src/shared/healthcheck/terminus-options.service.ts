import {
  TerminusEndpoint,
  TerminusOptionsFactory,
  TerminusModuleOptions,
} from '@nestjs/terminus';
import { Injectable } from '@nestjs/common';
import { EventStoreHealthIndicator } from './event-store.health';
import { EventStoreCatchupHealthIndicator } from './event-store-catchup.health';
import { TyepgooseHealthIndicator } from './typegoose.health';

@Injectable()
export class TerminusOptionsService implements TerminusOptionsFactory {
  constructor(
    private readonly typegooseHealthIndicator: TyepgooseHealthIndicator,
    private readonly eventStoreHealthIndicator: EventStoreHealthIndicator,
    private readonly eventStoreCatchupHealthIndicator: EventStoreCatchupHealthIndicator,
  ) {}

  createTerminusOptions(): TerminusModuleOptions {
    const healthEndpoint: TerminusEndpoint = {
      url: '/health',
      healthIndicators: [
        async () => await this.typegooseHealthIndicator.pingCheck('mongo'),
        async () => this.eventStoreHealthIndicator.isHealthy('eventStore'),
        async () =>
          this.eventStoreCatchupHealthIndicator.isHealthy('eventStoreBus'),
      ],
    };
    return {
      endpoints: [healthEndpoint],
    };
  }
}
