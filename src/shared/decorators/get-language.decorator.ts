import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const GetLanguage = createParamDecorator((_data, ctx: ExecutionContext): string => {
  try {
    const req = ctx.switchToHttp().getRequest();
    const acceptLanguageHeaderString = req.headers['accept-language'];
    const twoCharacters = 2;
    const firstLanguage = acceptLanguageHeaderString
      .split(',')[0]
      .trim()
      .substring(0, twoCharacters);
    return firstLanguage;
  } catch (error) {
    return 'en';
  }
});
