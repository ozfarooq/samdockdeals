import jwtDecode from 'jwt-decode';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class LoggedInUser {
  constructor(
    public _tenantID: string,
    public _id: string,
    public email: string,
    public isAdmin: boolean = false,
  ) {}

  generateMeta() {
    return new EventMetaData(this._tenantID, this._id);
  }
}

export function getUserFromJwt(bearerToken: string) {
  if (bearerToken && bearerToken.startsWith('Bearer ')) {
    const startPosition = 7;
    const token = bearerToken.substring(startPosition, bearerToken.length);
    try {
      const parsedUser: any = jwtDecode(token);
      const { _tenantID, _id, email, isAdmin } = parsedUser;
      return new LoggedInUser(_tenantID, _id, email, isAdmin);
    } catch (error) {
      throw new Error();
    }
  } else {
    throw new Error();
  }
}
