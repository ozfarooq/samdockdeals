import { BreadRepository } from '../shared/bread.respository';
import { Deal } from '../models/deal.model';
import { InjectModel } from 'nestjs-typegoose';
import { IDealStage } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ReturnModelType } from '@typegoose/typegoose/lib/types';
import { PROSPECTING_STAGE_ID, QUALIFIED_STAGE_ID } from '../models/pipeline.model';

export class DealRepository extends BreadRepository<Deal> {
  constructor(
    @InjectModel(Deal) dealModel: ReturnModelType<typeof Deal>,
  ) {
    super(dealModel);
  }

  async add(data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('CREATE DEAL');
    data._tenantID = meta._tenantID;
    console.table(data);
    try {
      if (!data.createdAt) data.createdAt = meta.timestamp;
      await this.model.create(data);
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'create',
        data._id,
        data,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }
  async browseDeals(meta: EventMetaData): Promise<Deal[]> {
    this.logger.verbose('FIND DEALS');
    try {
      const result = await this.model.find({
        _tenantID: meta._tenantID,
        $and: [
          { 'stage.stageID': { $ne: PROSPECTING_STAGE_ID } },
          { 'stage.stageID': { $ne: QUALIFIED_STAGE_ID } },
        ],
      },
                                           { __v: 0 },
      ).lean();
      return result;
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browseAll(meta: EventMetaData): Promise<Deal[]> {
    this.logger.verbose('FIND DEALS');
    try {
      const result = await this.model.find({
        _tenantID: meta._tenantID,
      },
      ).lean();
      return result;
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browseLeads(meta: EventMetaData): Promise<Deal[]> {
    this.logger.verbose('FIND LEADS');
    try {
      const result = await this.model.find(
        {
          _tenantID: meta._tenantID,
          $or: [
            { 'stage.stageID': PROSPECTING_STAGE_ID },
            { 'stage.stageID': QUALIFIED_STAGE_ID },
          ],
        },
        { __v: 0 },
      ).lean();
      return result;
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async relateOrganization(dealID, organizationID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $set: {
          organization: { _id: organizationID },
          updatedAt: meta.timestamp,
        } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'relateOrganization',
        dealID,
        organizationID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async unrelateOrganization(dealID, organizationID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $unset: { organization: '' }, $set: { updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'unrelateOrganization',
        dealID,
        organizationID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async relatePerson(dealID: string, personID: string, meta: EventMetaData): Promise<void> {
    try {
      await this.model.updateOne(
        {
          _tenantID: meta._tenantID,
          _id: dealID,
          stakeholders: { $ne: personID },
          contactPerson: { $ne: personID },
          budgetHolder: { $ne: personID },
          decisionMaker: { $ne: personID },
        },
        {
          $push: { stakeholders: { $each: [personID], $position: 0 } },
          $set: { updatedAt: meta.timestamp },
        },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'relatePerson',
        dealID,
        personID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async unrelatePerson(dealID, personID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $pull: { stakeholders: personID }, $set: { updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'unrelatePerson',
        dealID,
        personID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async assignContactPerson(dealID, personID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        {
          $set: { contactPerson: personID, updatedAt: meta.timestamp },
          $pull: { stakeholders: personID },
        },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'relatePerson',
        dealID,
        personID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async removeContactPerson(dealID, personID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $unset: { contactPerson: '' }, $set: { updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'unrelatePerson',
        dealID,
        personID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async changeStage(dealID: string, stage: IDealStage, meta: EventMetaData): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $set: { stage, updatedAt: meta.timestamp } },
      );
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  async assignBudget(dealID, budget, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $set: { budget, updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'assignBudget',
        dealID,
        JSON.stringify(budget),
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async removeBudget(dealID, meta): Promise<void> {
    try {
      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $unset: { budget: 1 }, $set: { updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'removeBudget',
        dealID,
        null,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async assignCustomerRequirement(dealID, customerRequirement, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $set: { customerRequirement, updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'assignCustomerRequirement',
        dealID,
        JSON.stringify(customerRequirement),
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async removeCustomerRequirement(dealID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $unset: { customerRequirement: 1 }, $set: { updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'removeCustomerRequirement',
        dealID,
        null,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async assignDecisionMaker(dealID, decisionMakerID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        {
          $set: { decisionMaker: decisionMakerID, updatedAt: meta.timestamp },
          $pull: { stakeholders: decisionMakerID },
        },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'assignDecisionMaker',
        dealID,
        decisionMakerID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async removeDecisionMaker(dealID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $unset: { decisionMaker: 1 }, $set: { updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'removeDecisionMaker',
        dealID,
        null,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async setTiming(dealID, timing, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $set: { timing, updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'setTiming',
        dealID,
        timing,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async removeTiming(dealID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $unset: { timing: 1 }, $set: { updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'removeTiming',
        dealID,
        null,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async assignBudgetHolder(dealID, budgetHolderID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        {
          $set: { budgetHolder: budgetHolderID, updatedAt: meta.timestamp },
          $pull: { stakeholders: budgetHolderID },
        },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'assignBudgetHolder',
        dealID,
        budgetHolderID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }
  async removeBudgetHolder(dealID, meta): Promise<void> {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $unset: { budgetHolder: 1 }, $set: { updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'removeBudgetHoler',
        dealID,
        null,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async archive(dealID: string, meta: EventMetaData) {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $set: { isArchived: true, updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'archive',
        dealID,
        null,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async reopen(dealID: string, meta: EventMetaData) {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: dealID },
        { $set: { isArchived: false, updatedAt: meta.timestamp } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'archive',
        dealID,
        null,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async updateMany(oldData, newData, meta: EventMetaData) {
    try {
      await this.model.updateMany(
        { _tenantID: meta._tenantID, ...oldData },
        { $set: newData },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'updateMany',
        null,
        { oldData, newData },
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

}
