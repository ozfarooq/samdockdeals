import { InjectModel } from 'nestjs-typegoose';
import { Pipeline, PipelineStage, FIRST_STAGE_ID, LOST_STAGE_ID, WON_STAGE_ID } from '../models/pipeline.model';
import { EventMetaData } from '@daypaio/domain-events/shared';
import {
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose/lib/types';
import { nanoid } from 'nanoid';
import { IUser } from '@daypaio/domain-events/users';
import { IPipeline, IPipelineStage } from '@daypaio/domain-events/deals';

const translations = {
  en: {
    qualifiedLeads: 'Qualified leads',
    confirmedRequirements: 'Confirmed requirements',
    offerSent: 'Offer sent',
    negotiation: 'Negotiation',
    won: 'Won',
    lost: 'Lost',
  },
  de: {
    qualifiedLeads: 'Qualifizierte Leads',
    confirmedRequirements: 'Anforderungen konkretisiert',
    offerSent: 'Angebot versendet',
    negotiation: 'Verhandlung',
    won: 'Gewonnen',
    lost: 'Verloren',
  },
};

export const getDefaultStages = (lang: string) => {
  return [
    {
      _id: FIRST_STAGE_ID,
      name: translations[lang].qualifiedLeads,
      order: [],
    },
    {
      _id: nanoid(),
      name: translations[lang].confirmedRequirements,
      order: [],
    },
    {
      _id: nanoid(),
      name: translations[lang].offerSent,
      order: [],
    },
    {
      _id: nanoid(),
      name: translations[lang].negotiation,
      order: [],
    },
    {
      _id: WON_STAGE_ID,
      name: translations[lang].won,
      order: [],
    },
  ] as PipelineStage[];
};

const FALLBACK_LANGUAGE = 'en';
const KNOWN_LANGUAGES = ['en', 'de'];

export class PipelineRepository {
  protected logger: Logger;
  constructor(
    @InjectModel(Pipeline) private model: ReturnModelType<typeof Pipeline>,
  ) {
    this.logger = new Logger(`${this.model.modelName}Repository`);
  }

  protected generateErrorMessage(
    error: any,
    operation: string,
    id?: string,
    data?: any,
  ) {
    const errorMessage = error.message;
    const operationMessage = `${
      this.model.modelName
    } could not be ${operation.toLowerCase()}ed}`;
    const idMessage = id ? `ID: ${id}` : '';
    const dataMessage = data ? JSON.stringify(data) : '';
    return {
      error: operationMessage + errorMessage,
      data: idMessage + dataMessage,
      verbose: `${error.constructor.name} \n
        ${operationMessage} \n
        ${errorMessage} \n
        ${idMessage} \n
        ${dataMessage}`,
    };
  }

  async add(pipeline: IPipeline, meta: EventMetaData) {
    await this.model.create({ ...pipeline, _tenantID: meta._tenantID });
  }

  async removePipelineStage(
    pipelineID: string,
    id: string,
    toStageID: string,
    meta: EventMetaData,
  ) {
    this.logger.verbose('REMOVE STAGE');
    console.table({ id });
    try {
      const pipeline = await this.model.findOne({ _tenantID: meta._tenantID, _id: pipelineID });
      const stages = [...pipeline.stages];
      const removedStageIndex = stages.findIndex(item => item._id === id);
      if (removedStageIndex === -1) {
        throw new NotFoundException(`Stage with that id:${id} is not found`);
      }
      const removedStage = stages.splice(removedStageIndex, 1)[0];
      if (removedStage.order?.length) {
        const toStageIndex = stages.findIndex(item => item._id === toStageID);
        if (toStageIndex === -1) {
          throw new NotFoundException(`Stage with that id:${id} is not found, param toStageID`);
        }
        stages[toStageIndex].order = [...stages[toStageIndex].order, ...removedStage.order];
      }
      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: pipelineID },
        { $set: { stages } },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'remove pipeline stage', id, null);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async addPipelineStage(pipelineID: string, stage: IPipelineStage, meta: EventMetaData) {
    this.logger.verbose('ADD STAGE');
    console.table({ stage });
    const id = stage._id;
    try {
      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: pipelineID },
        { $push: { stages: { $each: [stage], $position: -1 } } },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException('Update request was not successfull');
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'add stage', id, null);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async edit(id: string, data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('EDIT');
    console.table({ data, _id: id });
    try {
      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: id },
        { $set: data },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'updated', id, null);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async sortDeal(
    pipelineID: string | undefined,
    dealID: string,
    stageID: string,
    sortIndex: number,
    meta: EventMetaData,
  ) {
    await this.model.updateMany(
      { _tenantID: meta._tenantID, 'stages.order': { $in: dealID } },
      { $pull: { 'stages.$.order': dealID } },
    );

    if (!pipelineID || stageID === null || sortIndex === null) {
      return;
    }

    await this.model.updateOne(
      { _tenantID: meta._tenantID, _id: pipelineID, 'stages._id': stageID },
      {
        $push: {
          'stages.$.order': {
            $each: [dealID],
            $position: sortIndex,
          },
        },
      },
    );
  }

  async read(pipelineID: string, meta: EventMetaData) {
    const pipeline: Pipeline = await this.model.findOne(
      { _tenantID: meta._tenantID, _id: pipelineID },
    ).lean();

    return pipeline;
  }

  async browse(meta: EventMetaData) {
    const pipelines: Pipeline = await this.model.find(
      { _tenantID: meta._tenantID },
    ).sort({ createdAt: '1' }).lean();

    return pipelines;
  }

  async addDefaultPipeline(user: IUser, meta: EventMetaData) {
    if (!user.isSignupUser) return;
    let language = FALLBACK_LANGUAGE;
    if (user?.language && KNOWN_LANGUAGES.includes(user?.language)) {
      language = user?.language;
    } else if (user?.initialBrowserLanguage &&
      KNOWN_LANGUAGES.includes(user?.initialBrowserLanguage)) {
      language = user?.initialBrowserLanguage;
    }
    const defaultStages = getDefaultStages(language);
    const defaultPipeline: Pipeline = {
      _id: meta._tenantID,
      _tenantID: meta._tenantID,
      name: 'Pipeline',
      stages: defaultStages,
      createdAt: Date.now(),
    };
    await this.model.create(defaultPipeline);
  }

  async renamePipelineStage(pipelineID: string, id: string, name: string, meta: EventMetaData) {
    return this.model.updateOne(
      { _tenantID: meta._tenantID, _id: pipelineID, 'stages._id': id },
      { $set: { 'stages.$.name': name } },
    );
  }

  async delete(
    id: string,
    toPipelineID: string,
    toStageID: string,
    meta: EventMetaData,
  ): Promise<void> {
    if (toPipelineID && toStageID) {
      const oldPipeline = await this.read(id, meta);
      const dealsToMigrate = oldPipeline.stages.reduce(
        (acc, stage) => [...acc, ...stage.order],
        [],
      );
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: toPipelineID, 'stages._id': toStageID },
        {
          $push: {
            'stages.$.order': {
              $each: dealsToMigrate,
            },
          },
        },
      );
    }
    await this.model.deleteOne({
      _tenantID: meta._tenantID,
      _id: id,
    });
  }

  async deleteAll(meta: EventMetaData): Promise<void> {
    await this.model.deleteMany({
      _tenantID: meta._tenantID,
    });
  }
}
