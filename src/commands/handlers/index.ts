import { AddDealCommandHandler } from './add-deal.command-handler';
import { DeleteDealCommandHandler } from './delete-deal.command-handler';
import { EditDealCommandHandler } from './edit-deal.command-handler';
import { RenamePipelineStageCommandHandler } from './rename-pipeline-stage.command-handler';
import { RelateOrganizationToDealCommandHandler } from './relate-organization-to-deal.command-handler';
import { UnrelateOrganizationFromDealCommandHandler } from './unrelate-organization-from-deal.command-handler';
import { RelatePersonToDealCommandHandler } from './relate-person-to-deal.command-handler';
import { UnrelatePersonFromDealCommandHandler } from './unrelate-person-from-deal.command-handler';
import { AssignContactPersonToDealCommandHandler } from './assign-contact-person-to-deal.command-handler';
import { RemoveContactPersonFromDealCommandHandler } from './remove-contact-person-from-deal.command-handler';
import { ChangeDealStageCommandHandler } from './change-deal-stage.command-handler';
import { CreateLeadFromDraftCommandHandler } from './create-lead-from-draft.command-handler';
import { AllocateBudgetToDealCommandHandler } from './allocate-budget-to-deal.command-handler';
import { AssignDecisionMakerToDealCommandHandler } from './assign-decision-maker-to-deal.command-handler';
import { RemoveDecisionMakerFromDealCommandHandler } from './remove-decision-maker-from-deal.command-handler';
import { ChangeCustomerRequirementForDealCommandHandler } from './change-customer-requirement-for-deal.command-handler';
import { ChangeTimingForDealCommandHandler } from './change-timing-for-deal.command-handler';
import { AssignBudgeHolderToDealCommandHandler } from './assign-budget-holder-to-deal.command-handler';
import { RemoveBudgeHolderFromDealCommandHandler } from './remove-budget-holder-from-deal.command-handler';
import { ChangeLeadQualificationCriteriaCommandHandler } from './change-lead-qualification-criteria.command-handler';
import { QualifyLeadCommandHandler } from './qualify-lead.command-handler';
import { PromoteLeadToDealCommandHandler } from './promote-lead-to-deal.command-handler';
import { DemoteDealToLeadCommandHandler } from './demote-deal-to-lead.command-handler';
import { RelatePersonToLeadCommandHandler } from './relate-person-to-lead.command-handler';
import { UnrelatePersonFromLeadCommandHandler } from './unrelate-person-from-lead.command-handler';
import { RelateOrganizationToLeadCommandHandler } from './relate-organization-to-lead.command-handler';
import { UnrelateOrganizationFromLeadCommandHandler } from './unrelate-organization-from-lead.command-handler';
import { DeletePipelineCommandHandler } from './delete-pipeline.command-handler';
import { AddPipelineCommandHandler } from './add-pipeline.command-handler';
import { SortDealCommandCommandHandler } from './sort-deal.command-handler';
import { AddLeadCommandHandler } from './add-lead.command-handler';
import { AllocateBudgetToLeadCommandHandler } from './allocate-budget-to-lead.command-handler';
import { AssignBudgeHolderToLeadCommandHandler } from './assign-budget-holder-to-lead.command-handler';
import { AssignDecisionMakerToLeadCommandHandler } from './assign-decision-maker-to-lead.command-handler';
import { ChangeCustomerRequirementForLeadCommandHandler } from './change-customer-requirement-for-lead.command-handler';
import { ChangeTimingForLeadCommandHandler } from './change-timing-for-lead.command-handler';
import { DeleteLeadCommandHandler } from './delete-lead.command-handler';
import { RemoveBudgeHolderFromLeadCommandHandler } from './remove-budget-holder-from-lead.command-handler';
import { RemoveContactPersonFromLeadCommandHandler } from './remove-contact-person-from-lead.command-handler';
import { RemoveDecisionMakerFromLeadCommandHandler } from './remove-decision-maker-from-lead.command-handler';
import { EditLeadCommandHandler } from './edit-lead.command-handler';
import { AssignContactPersonToLeadCommandHandler } from './assign-contact-person-to-lead.command-handler';
import { UnqualifyLeadCommandHandler } from './unqualify-lead.command-handler';
import { ArchiveDealCommandHandler } from './archive-deal.command-handler';
import { ArchiveLeadCommandHandler } from './archive-lead.command-handler';
import { ReopenDealCommandHandler } from './reopen-deal.command-handler';
import { ReopenLeadCommandHandler } from './reopen-lead.command-handler';
import { AddPipelineStageCommandHandler } from './add-pipeline-stage.command-handler';
import { RemovePipelineStageCommandHandler } from './remove-pipeline-stage.command-handler';
import { EditPipelineCommandHandler } from './edit-pipeline.command-handler';

export const DealCommandHandlers = [
  AddDealCommandHandler,
  DeleteDealCommandHandler,
  EditDealCommandHandler,
  RenamePipelineStageCommandHandler,
  RelateOrganizationToDealCommandHandler,
  UnrelateOrganizationFromDealCommandHandler,
  RelatePersonToDealCommandHandler,
  UnrelatePersonFromDealCommandHandler,
  AssignContactPersonToDealCommandHandler,
  RemoveContactPersonFromDealCommandHandler,
  ChangeDealStageCommandHandler,
  CreateLeadFromDraftCommandHandler,
  AllocateBudgetToDealCommandHandler,
  AssignDecisionMakerToDealCommandHandler,
  RemoveDecisionMakerFromDealCommandHandler,
  ChangeCustomerRequirementForDealCommandHandler,
  ChangeTimingForDealCommandHandler,
  AssignBudgeHolderToDealCommandHandler,
  RemoveBudgeHolderFromDealCommandHandler,
  ChangeLeadQualificationCriteriaCommandHandler,
  QualifyLeadCommandHandler,
  PromoteLeadToDealCommandHandler,
  DemoteDealToLeadCommandHandler,
  RelatePersonToLeadCommandHandler,
  UnrelatePersonFromLeadCommandHandler,
  RelateOrganizationToLeadCommandHandler,
  UnrelateOrganizationFromLeadCommandHandler,
  DeletePipelineCommandHandler,
  AddPipelineCommandHandler,
  SortDealCommandCommandHandler,
  AddLeadCommandHandler,
  AllocateBudgetToLeadCommandHandler,
  AssignBudgeHolderToLeadCommandHandler,
  AssignDecisionMakerToLeadCommandHandler,
  AssignContactPersonToLeadCommandHandler,
  ChangeCustomerRequirementForLeadCommandHandler,
  ChangeTimingForLeadCommandHandler,
  DeleteLeadCommandHandler,
  RemoveBudgeHolderFromLeadCommandHandler,
  RemoveContactPersonFromLeadCommandHandler,
  RemoveDecisionMakerFromLeadCommandHandler,
  EditLeadCommandHandler,
  UnqualifyLeadCommandHandler,
  ArchiveLeadCommandHandler,
  ArchiveDealCommandHandler,
  ReopenLeadCommandHandler,
  ReopenDealCommandHandler,
  AddPipelineStageCommandHandler,
  RemovePipelineStageCommandHandler,
  EditPipelineCommandHandler,
];
