import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { TimingChangedForDealEvent } from '@daypaio/domain-events/deals';
import { ChangeTimingForDealCommand } from '../impl/change-timing-for-deal.command';

@CommandHandler(ChangeTimingForDealCommand)
export class ChangeTimingForDealCommandHandler
  implements ICommandHandler<ChangeTimingForDealCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: ChangeTimingForDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, timestamp, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new TimingChangedForDealEvent(_dealID, timestamp, meta));
    dealAggregate.commit();
  }
}
