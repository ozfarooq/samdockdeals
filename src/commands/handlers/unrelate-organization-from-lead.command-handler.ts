import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { UnrelateOrganizationFromLeadCommand } from '../impl/unrelate-organization-from-lead.command';
import { OrganizationUnrelatedFromLeadEvent } from '@daypaio/domain-events/deals';

@CommandHandler(UnrelateOrganizationFromLeadCommand)
export class UnrelateOrganizationFromLeadCommandHandler
  implements ICommandHandler<UnrelateOrganizationFromLeadCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelateOrganizationFromLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { leadID, organizationID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: leadID }),
    );
    dealAggregate.apply(new OrganizationUnrelatedFromLeadEvent(leadID, organizationID, meta));
    dealAggregate.commit();
  }
}
