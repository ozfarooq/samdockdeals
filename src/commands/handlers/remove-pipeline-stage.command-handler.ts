import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { RemovePipelineStageCommand } from '../impl/remove-pipeline-stage.command';
import { EventPublisher } from 'nestjs-eventstore';
import { PipelineAggregate } from '../../models/pipeline.aggregate';
import { PipelineStageRemovedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(RemovePipelineStageCommand)
export class RemovePipelineStageCommandHandler
  implements ICommandHandler<RemovePipelineStageCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RemovePipelineStageCommand) {
    this.logger.log('COMMAND TRIGGERED: RemovePipelineStageCommand...');
    const { pipelineID, id, toStageID, meta } = command;
    const pipeline = this.publisher.mergeObjectContext(
      new PipelineAggregate({ _id: meta._tenantID }),
    );
    pipeline.apply(new PipelineStageRemovedEvent(pipelineID, id, toStageID, meta));
    pipeline.commit();
  }
}
