import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RelateOrganizationToDealCommand } from '../impl/relate-organization-to-deal.command';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { OrganizationRelatedToDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(RelateOrganizationToDealCommand)
export class RelateOrganizationToDealCommandHandler
  implements ICommandHandler<RelateOrganizationToDealCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelateOrganizationToDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { dealID, organizationID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: dealID }),
    );
    dealAggregate.apply(new OrganizationRelatedToDealEvent(dealID, organizationID, meta));
    dealAggregate.commit();
  }
}
