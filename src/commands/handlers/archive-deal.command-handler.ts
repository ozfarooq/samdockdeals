import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EventPublisher } from 'nestjs-eventstore';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { ArchiveDealCommand } from '../impl/archive-deal.command';
import { DealArchivedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(ArchiveDealCommand)
export class ArchiveDealCommandHandler
  implements ICommandHandler<ArchiveDealCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) { }

  async execute(command: ArchiveDealCommand) {
    this.logger.log('COMMAND TRIGGERED...');
    const { _dealID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new DealArchivedEvent(_dealID, meta));
    dealAggregate.commit();
  }

}
