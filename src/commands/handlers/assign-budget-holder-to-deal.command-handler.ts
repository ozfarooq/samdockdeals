import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AssignBudgetHolderToDealCommand } from '../impl/assign-budget-holder-to-deal.command';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { BudgetHolderAssignedToDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(AssignBudgetHolderToDealCommand)
export class AssignBudgeHolderToDealCommandHandler
  implements ICommandHandler<AssignBudgetHolderToDealCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AssignBudgetHolderToDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, budgetHolderID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new BudgetHolderAssignedToDealEvent(_dealID, budgetHolderID, meta));
    dealAggregate.commit();
  }
}
