import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { UnrelatePersonFromDealCommand } from '../impl/unrelate-person-from-deal.command';
import { PersonUnrelatedFromDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(UnrelatePersonFromDealCommand)
export class UnrelatePersonFromDealCommandHandler
  implements ICommandHandler<UnrelatePersonFromDealCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelatePersonFromDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { dealID, personID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: dealID }),
    );
    dealAggregate.apply(new PersonUnrelatedFromDealEvent(dealID, personID, meta));
    dealAggregate.commit();
  }
}
