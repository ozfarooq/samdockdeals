import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EventPublisher } from 'nestjs-eventstore';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { LeadReopenedEvent } from '@daypaio/domain-events/deals';
import { ReopenLeadCommand } from '../impl/reopen-lead.command';

@CommandHandler(ReopenLeadCommand)
export class ReopenLeadCommandHandler
  implements ICommandHandler<ReopenLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) { }

  async execute(command: ReopenLeadCommand) {
    this.logger.log('COMMAND TRIGGERED...');
    const { _leadID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _leadID }),
    );
    dealAggregate.apply(new LeadReopenedEvent(_leadID, meta));
    dealAggregate.commit();
  }

}
