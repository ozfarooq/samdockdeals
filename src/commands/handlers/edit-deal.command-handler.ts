import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EditDealCommand } from '../impl/edit-deal.command';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { DealEditedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(EditDealCommand)
export class EditDealCommandHandler
  implements ICommandHandler<EditDealCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: EditDealCommand) {
    this.logger.log('COMMAND TRIGGERED: EditDealCommandHandler...');
    const { id, deal, meta } = command;
    deal._id = id;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate(deal),
    );
    dealAggregate.apply(new DealEditedEvent(id, deal, meta));
    dealAggregate.commit();
  }
}
