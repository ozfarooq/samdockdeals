import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EventPublisher } from 'nestjs-eventstore';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { UnqualifyLeadCommand } from '../impl/unqualify-lead.command';
import { LeadUnqualifiedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(UnqualifyLeadCommand)
export class UnqualifyLeadCommandHandler
  implements ICommandHandler<UnqualifyLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) { }

  async execute(command: UnqualifyLeadCommand) {
    this.logger.log('COMMAND TRIGGERED...');
    const { _leadID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _leadID }),
    );
    dealAggregate.apply(new LeadUnqualifiedEvent(_leadID, meta));
    dealAggregate.commit();
  }

}
