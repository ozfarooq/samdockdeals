import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { DecisionMakerRemovedFromDealEvent } from '@daypaio/domain-events/deals';
import {
  RemoveDecisionMakerFromDealCommand,
} from '../impl/remove-decision-maker-from-deal.command';

@CommandHandler(RemoveDecisionMakerFromDealCommand)
export class RemoveDecisionMakerFromDealCommandHandler
  implements ICommandHandler<RemoveDecisionMakerFromDealCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveDecisionMakerFromDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID,  meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new DecisionMakerRemovedFromDealEvent(
      _dealID, meta),
    );
    dealAggregate.commit();
  }
}
