import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { BudgetAllocatedForLeadEvent } from '@daypaio/domain-events/deals';
import { AllocateBudgetToLeadCommand } from '../impl/allocate-budget-to-lead.command';

@CommandHandler(AllocateBudgetToLeadCommand)
export class AllocateBudgetToLeadCommandHandler
  implements ICommandHandler<AllocateBudgetToLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AllocateBudgetToLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, budget, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new BudgetAllocatedForLeadEvent(_dealID, budget, meta));
    dealAggregate.commit();
  }
}
