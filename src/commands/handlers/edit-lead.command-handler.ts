import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { LeadEditedEvent } from '@daypaio/domain-events/deals';
import { EditLeadCommand } from '../impl/edit-lead.command';

@CommandHandler(EditLeadCommand)
export class EditLeadCommandHandler
  implements ICommandHandler<EditLeadCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: EditLeadCommand) {
    this.logger.log('COMMAND TRIGGERED: EditLeadCommandHandler...');
    const { id, deal, meta } = command;
    deal._id = id;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate(deal),
    );
    dealAggregate.apply(new LeadEditedEvent(id, deal, meta));
    dealAggregate.commit();
  }
}
