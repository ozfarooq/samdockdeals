import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { RelateOrganizationToLeadCommand } from '../impl/relate-organization-to-lead.command';
import { OrganizationRelatedToLeadEvent } from '@daypaio/domain-events/deals';

@CommandHandler(RelateOrganizationToLeadCommand)
export class RelateOrganizationToLeadCommandHandler
  implements ICommandHandler<RelateOrganizationToLeadCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelateOrganizationToLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { leadID, organizationID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: leadID }),
    );
    dealAggregate.apply(new OrganizationRelatedToLeadEvent(leadID, organizationID, meta));
    dealAggregate.commit();
  }
}
