import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { AddDealCommand } from '../impl/add-deal.command';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { DealAddedEvent } from '@daypaio/domain-events/deals';
import { LOST_STAGE_ID } from '../../models/pipeline.model';

@CommandHandler(AddDealCommand)
export class AddDealCommandHandler
  implements ICommandHandler<AddDealCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AddDealCommand) {
    this.logger.log('COMMAND TRIGGERED: AddDealCommandHandler...');
    const { deal, meta } = command;

    deal.isArchived = deal.stage?.stageID === LOST_STAGE_ID;

    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate(deal),
    );
    dealAggregate.apply(new DealAddedEvent(deal._id, deal, meta));
    dealAggregate.commit();
  }
}
