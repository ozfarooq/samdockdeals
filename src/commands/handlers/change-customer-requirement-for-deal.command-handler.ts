import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ChangeCustomerRequirementForDealCommand } from '../impl/change-customer-requirement-for-deal.command';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { CustomerRequirementChangedForDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(ChangeCustomerRequirementForDealCommand)
export class ChangeCustomerRequirementForDealCommandHandler
  implements ICommandHandler<ChangeCustomerRequirementForDealCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: ChangeCustomerRequirementForDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, customerRequirement, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new CustomerRequirementChangedForDealEvent(
        _dealID, customerRequirement, meta),
    );
    dealAggregate.commit();
  }
}
