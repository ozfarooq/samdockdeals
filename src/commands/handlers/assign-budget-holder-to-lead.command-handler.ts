import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { BudgetHolderAssignedToLeadEvent } from '@daypaio/domain-events/deals';
import { AssignBudgetHolderToLeadCommand } from '../impl/assign-budget-holder-to-lead.command';

@CommandHandler(AssignBudgetHolderToLeadCommand)
export class AssignBudgeHolderToLeadCommandHandler
  implements ICommandHandler<AssignBudgetHolderToLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AssignBudgetHolderToLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, budgetHolderID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new BudgetHolderAssignedToLeadEvent(_dealID, budgetHolderID, meta));
    dealAggregate.commit();
  }
}
