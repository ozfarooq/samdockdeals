import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { CustomerRequirementChangedForLeadEvent } from '@daypaio/domain-events/deals';
import { ChangeCustomerRequirementForLeadCommand } from '../impl/change-customer-requirement-for-lead.command';

@CommandHandler(ChangeCustomerRequirementForLeadCommand)
export class ChangeCustomerRequirementForLeadCommandHandler
  implements ICommandHandler<ChangeCustomerRequirementForLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: ChangeCustomerRequirementForLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, customerRequirement, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new CustomerRequirementChangedForLeadEvent(
        _dealID, customerRequirement, meta),
    );
    dealAggregate.commit();
  }
}
