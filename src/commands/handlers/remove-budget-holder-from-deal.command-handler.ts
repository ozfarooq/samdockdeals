import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { BudgetHolderRemovedFromDealEvent } from '@daypaio/domain-events/deals';
import { RemoveBudgetHolderFromDealCommand } from '../impl/remove-budget-holder-from-deal.command';

@CommandHandler(RemoveBudgetHolderFromDealCommand)
export class RemoveBudgeHolderFromDealCommandHandler
  implements ICommandHandler<RemoveBudgetHolderFromDealCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveBudgetHolderFromDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new BudgetHolderRemovedFromDealEvent(_dealID, meta));
    dealAggregate.commit();
  }
}
