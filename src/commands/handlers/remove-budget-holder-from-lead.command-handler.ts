import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { BudgetHolderRemovedFromLeadEvent } from '@daypaio/domain-events/deals';
import { RemoveBudgetHolderFromLeadCommand } from '../impl/remove-budget-holder-from-lead.command';

@CommandHandler(RemoveBudgetHolderFromLeadCommand)
export class RemoveBudgeHolderFromLeadCommandHandler
  implements ICommandHandler<RemoveBudgetHolderFromLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveBudgetHolderFromLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new BudgetHolderRemovedFromLeadEvent(_dealID, meta));
    dealAggregate.commit();
  }
}
