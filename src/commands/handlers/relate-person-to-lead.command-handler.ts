import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { RelatePersonToLeadCommand } from '../impl/relate-person-to-lead.command';
import { PersonRelatedToLeadEvent } from '@daypaio/domain-events/deals';

@CommandHandler(RelatePersonToLeadCommand)
export class RelatePersonToLeadCommandHandler
  implements ICommandHandler<RelatePersonToLeadCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelatePersonToLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { leadID, personID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: leadID }),
    );
    const event = new PersonRelatedToLeadEvent(leadID, personID, meta);
    dealAggregate.apply(event);
    dealAggregate.commit();
  }
}
