import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EventPublisher } from 'nestjs-eventstore';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { ReopenDealCommand } from '../impl/reopen-deal.command';
import { DealReopenedEvent, DealSortedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(ReopenDealCommand)
export class ReopenDealCommandHandler
  implements ICommandHandler<ReopenDealCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) { }

  async execute(command: ReopenDealCommand) {
    this.logger.log('COMMAND TRIGGERED...');
    const { pipelineID, _dealID, stageID, meta } = command;

    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new DealReopenedEvent(_dealID, meta));
    dealAggregate.apply(new DealSortedEvent(_dealID, pipelineID, stageID, 0, meta));
    dealAggregate.commit();
  }

}
