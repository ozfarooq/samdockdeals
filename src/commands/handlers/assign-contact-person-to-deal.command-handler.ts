import { AssignContactPersonToDealCommand } from '../impl/assign-contact-person-to-deal.command';
import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { ContactPersonAssignedToDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(AssignContactPersonToDealCommand)
export class AssignContactPersonToDealCommandHandler
  implements ICommandHandler<AssignContactPersonToDealCommand> {
  private logger: Logger;
  constructor(
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AssignContactPersonToDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, _personID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new ContactPersonAssignedToDealEvent(_dealID, _personID, meta));
    dealAggregate.commit();
  }
}
