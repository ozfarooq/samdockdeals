import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { RenamePipelineStageCommand } from '../impl/rename-pipeline-stage.command';
import { EventPublisher } from 'nestjs-eventstore';
import { PipelineAggregate } from '../../models/pipeline.aggregate';

@CommandHandler(RenamePipelineStageCommand)
export class RenamePipelineStageCommandHandler
  implements ICommandHandler<RenamePipelineStageCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RenamePipelineStageCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { pipelineID, stageID, newName, meta } = command;
    const pipeline = this.publisher.mergeObjectContext(
      new PipelineAggregate({ _id: meta._tenantID }),
    );
    pipeline.renamePipelineStage(pipelineID, stageID, newName, meta);
    pipeline.commit();
  }
}
