import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DeleteDealCommand } from '../impl/delete-deal.command';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { DealDeletedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(DeleteDealCommand)
export class DeleteDealCommandHandler
  implements ICommandHandler<DeleteDealCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: DeleteDealCommand) {
    this.logger.log('COMMAND TRIGGERED: DeleteDealCommandHandler...');
    const { id, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: id }),
    );
    dealAggregate.apply(new DealDeletedEvent(id, meta));
    dealAggregate.commit();
  }
}
