import { QualifyLeadCommand } from '../impl/qualify-lead.command';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EventPublisher } from 'nestjs-eventstore';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { LeadQualifiedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(QualifyLeadCommand)
export class QualifyLeadCommandHandler
  implements ICommandHandler<QualifyLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) { }

  async execute(command: QualifyLeadCommand) {
    this.logger.log('COMMAND TRIGGERED...');
    const { _leadID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _leadID }),
    );
    dealAggregate.apply(new LeadQualifiedEvent(_leadID, meta));
    dealAggregate.commit();
  }

}
