import {
    RemoveContactPersonFromLeadCommand,
} from '../impl/remove-contact-person-from-lead.command';
import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { ContactPersonRemovedFromLeadEvent } from '@daypaio/domain-events/deals';

@CommandHandler(RemoveContactPersonFromLeadCommand)
export class RemoveContactPersonFromLeadCommandHandler
  implements ICommandHandler<RemoveContactPersonFromLeadCommand> {
  private logger: Logger;
  constructor(
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RemoveContactPersonFromLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, _personID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new ContactPersonRemovedFromLeadEvent(_dealID, _personID, meta));
    dealAggregate.commit();
  }
}
