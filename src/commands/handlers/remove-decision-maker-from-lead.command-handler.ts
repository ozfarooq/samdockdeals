import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { DecisionMakerRemovedFromLeadEvent } from '@daypaio/domain-events/deals';
import {
  RemoveDecisionMakerFromLeadCommand,
} from '../impl/remove-decision-maker-from-lead.command';

@CommandHandler(RemoveDecisionMakerFromLeadCommand)
export class RemoveDecisionMakerFromLeadCommandHandler
  implements ICommandHandler<RemoveDecisionMakerFromLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveDecisionMakerFromLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID,  meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new DecisionMakerRemovedFromLeadEvent(
      _dealID, meta),
    );
    dealAggregate.commit();
  }
}
