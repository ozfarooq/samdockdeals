import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { LeadAddedEvent } from '@daypaio/domain-events/deals';
import { AddLeadCommand } from '../impl/add-lead.command';

@CommandHandler(AddLeadCommand)
export class AddLeadCommandHandler
  implements ICommandHandler<AddLeadCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AddLeadCommand) {
    this.logger.log('COMMAND TRIGGERED: AddLeadCommandHandler...');
    const { deal, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate(deal),
    );
    dealAggregate.apply(new LeadAddedEvent(deal._id, deal, meta));
    dealAggregate.commit();
  }
}
