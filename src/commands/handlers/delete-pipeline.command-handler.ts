import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DeletePipelineCommand } from '../impl/delete-pipeline.command';
import { EventPublisher } from 'nestjs-eventstore';
import { PipelineAggregate } from '../../models/pipeline.aggregate';

@CommandHandler(DeletePipelineCommand)
export class DeletePipelineCommandHandler
  implements ICommandHandler<DeletePipelineCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: DeletePipelineCommand) {
    this.logger.log('COMMAND TRIGGERED: DeletePipelineCommandHandler...');
    const { id, toPipelineID, toStageID, meta } = command;
    const settings = this.publisher.mergeObjectContext(
      new PipelineAggregate({ _id: id }),
    );
    settings.delete(toPipelineID, toStageID, meta);
    settings.commit();
  }
}
