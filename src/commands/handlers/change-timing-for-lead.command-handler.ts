import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { TimingChangedForLeadEvent } from '@daypaio/domain-events/deals';
import { ChangeTimingForLeadCommand } from '../impl/change-timing-for-lead.command';

@CommandHandler(ChangeTimingForLeadCommand)
export class ChangeTimingForLeadCommandHandler
  implements ICommandHandler<ChangeTimingForLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: ChangeTimingForLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, timestamp, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new TimingChangedForLeadEvent(_dealID, timestamp, meta));
    dealAggregate.commit();
  }
}
