import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AddPipelineCommand } from '../impl/add-pipeline.command';
import { PipelineAggregate } from '../../models/pipeline.aggregate';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { PipelineAddedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(AddPipelineCommand)
export class AddPipelineCommandHandler
  implements ICommandHandler<AddPipelineCommand> {

  private logger: Logger;

  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AddPipelineCommand) {
    this.logger.log('COMMAND TRIGGERED: AddPipelineCommandHandler...');
    const { pipeline, meta } = command;
    if (!pipeline.createdAt) {
      pipeline.createdAt = Date.now();
    }
    const pipelineAggregate = this.publisher.mergeObjectContext(
      new PipelineAggregate(pipeline),
    );
    pipelineAggregate.apply(new PipelineAddedEvent(pipeline, meta));
    pipelineAggregate.commit();
  }
}
