import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AssignDecisionMakerToDealCommand } from '../impl/assign-decision-maker-to-deal.command';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { DecisionMakerAssignedToDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(AssignDecisionMakerToDealCommand)
export class AssignDecisionMakerToDealCommandHandler
  implements ICommandHandler<AssignDecisionMakerToDealCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AssignDecisionMakerToDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, decisionMakerID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new DecisionMakerAssignedToDealEvent(_dealID, decisionMakerID, meta));
    dealAggregate.commit();
  }
}
