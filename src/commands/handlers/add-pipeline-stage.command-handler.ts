import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { AddPipelineStageCommand } from '../impl/add-pipeline-stage.command';
import { EventPublisher } from 'nestjs-eventstore';
import { PipelineAggregate } from '../../models/pipeline.aggregate';
import { PipelineStageAddedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(AddPipelineStageCommand)
export class AddPipelineStageCommandHandler
  implements ICommandHandler<AddPipelineStageCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AddPipelineStageCommand) {
    this.logger.log('COMMAND TRIGGERED: AddPipelineStageCommand...');
    const { pipelineID, stage, meta } = command;
    const pipeline = this.publisher.mergeObjectContext(
      new PipelineAggregate({ _id: meta._tenantID }),
    );
    pipeline.apply(new PipelineStageAddedEvent(pipelineID, stage, meta));
    pipeline.commit();
  }
}
