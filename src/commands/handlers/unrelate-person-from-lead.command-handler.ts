import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { UnrelatePersonFromLeadCommand } from '../impl/unrelate-person-to-lead.command';
import { PersonUnrelatedFromLeadEvent } from '@daypaio/domain-events/deals';

@CommandHandler(UnrelatePersonFromLeadCommand)
export class UnrelatePersonFromLeadCommandHandler
  implements ICommandHandler<UnrelatePersonFromLeadCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelatePersonFromLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { leadID, personID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: leadID }),
    );
    dealAggregate.apply(new PersonUnrelatedFromLeadEvent(leadID, personID, meta));
    dealAggregate.commit();
  }
}
