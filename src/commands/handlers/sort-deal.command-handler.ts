import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { SortDealCommand } from '../impl/sort-deal.command';
import { DealSortedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(SortDealCommand)
export class SortDealCommandCommandHandler
  implements ICommandHandler<SortDealCommand> {
  private logger: Logger;

  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: SortDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { pipelineID, dealID, stageID, sortIndex, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: dealID }),
    );
    dealAggregate.apply(new DealSortedEvent(dealID, pipelineID, stageID, sortIndex, meta));
    dealAggregate.commit();
  }
}
