import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AllocateBudgetToDealCommand } from '../impl/allocate-budget-to-deal.command';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { BudgetAllocatedForDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(AllocateBudgetToDealCommand)
export class AllocateBudgetToDealCommandHandler
  implements ICommandHandler<AllocateBudgetToDealCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AllocateBudgetToDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, budget, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new BudgetAllocatedForDealEvent(_dealID, budget, meta));
    dealAggregate.commit();
  }
}
