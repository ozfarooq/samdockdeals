import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { LeadDeletedEvent } from '@daypaio/domain-events/deals';
import { DeleteLeadCommand } from '../impl/delete-lead.command';

@CommandHandler(DeleteLeadCommand)
export class DeleteLeadCommandHandler
  implements ICommandHandler<DeleteLeadCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: DeleteLeadCommand) {
    this.logger.log('COMMAND TRIGGERED: DeleteLeadCommandHandler...');
    const { id, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: id }),
    );
    dealAggregate.apply(new LeadDeletedEvent(id, meta));
    dealAggregate.commit();
  }
}
