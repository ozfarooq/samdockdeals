import { CommandHandler } from '@nestjs/cqrs';
import { PromoteLeadToDealCommand } from '../impl/promote-lead-to-deal.command';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { LeadPromotedToDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(PromoteLeadToDealCommand)
export class PromoteLeadToDealCommandHandler {

  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: PromoteLeadToDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { pipelineID, id, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: id }),
    );
    dealAggregate.apply(new LeadPromotedToDealEvent(id, pipelineID, meta));
    dealAggregate.commit();
  }

}
