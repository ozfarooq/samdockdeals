import { CommandHandler, ICommandHandler, EventPublisher, IEvent } from '@nestjs/cqrs';
import { ChangeLeadQualificationCriteriaCommand } from '../impl/change-lead-qualification-criteria.command';
import { Logger } from '@nestjs/common';
import { DealRepository } from '../../repositories/deals.repository';
import { DealAggregate } from '../../models/deal.aggregate';
import { interval } from 'rxjs';
import { first } from 'rxjs/operators';
import { DealStageChangedEvent } from '@daypaio/domain-events/deals';
import { PROSPECTING_STAGE_ID, QUALIFIED_STAGE_ID } from '../../models/pipeline.model';

@CommandHandler(ChangeLeadQualificationCriteriaCommand)
export class ChangeLeadQualificationCriteriaCommandHandler
  implements ICommandHandler<ChangeLeadQualificationCriteriaCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
    private repo: DealRepository,
  ) { }

  async execute(command: ChangeLeadQualificationCriteriaCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, meta } = command;
    await interval(1000).pipe(first()).toPromise();
    const lead = await this.repo.read(_dealID, meta);
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate(lead),
    );

    if (dealAggregate.isQualified && dealAggregate.stage.stageID === PROSPECTING_STAGE_ID) {
      dealAggregate.apply(
        new DealStageChangedEvent(_dealID, { stageID: QUALIFIED_STAGE_ID, pipelineID: null }, meta),
      );
      dealAggregate.commit();
    } else {
      this.logger.verbose('Lead changed but did not qualify to be promoted or demoted');
    }

  }
}
