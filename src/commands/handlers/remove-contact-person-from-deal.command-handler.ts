import {
    RemoveContactPersonFromDealCommand,
} from '../impl/remove-contact-person-from-deal.command';
import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { ContactPersonRemovedFromDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(RemoveContactPersonFromDealCommand)
export class RemoveContactPersonFromDealCommandHandler
  implements ICommandHandler<RemoveContactPersonFromDealCommand> {
  private logger: Logger;
  constructor(
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RemoveContactPersonFromDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, _personID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new ContactPersonRemovedFromDealEvent(_dealID, _personID, meta));
    dealAggregate.commit();
  }
}
