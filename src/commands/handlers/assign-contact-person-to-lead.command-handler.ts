import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { ContactPersonAssignedToLeadEvent } from '@daypaio/domain-events/deals';
import { AssignContactPersonToLeadCommand } from '../impl/assign-contact-person-to-lead.command';

@CommandHandler(AssignContactPersonToLeadCommand)
export class AssignContactPersonToLeadCommandHandler
  implements ICommandHandler<AssignContactPersonToLeadCommand> {
  private logger: Logger;
  constructor(
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AssignContactPersonToLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, _personID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new ContactPersonAssignedToLeadEvent(_dealID, _personID, meta));
    dealAggregate.commit();
  }
}
