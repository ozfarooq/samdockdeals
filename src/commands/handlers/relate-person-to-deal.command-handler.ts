import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { RelatePersonToDealCommand } from '../impl/relate-person-to-deal.command';
import { PersonRelatedToDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(RelatePersonToDealCommand)
export class RelatePersonToDealCommandHandler
  implements ICommandHandler<RelatePersonToDealCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelatePersonToDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { dealID, personID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: dealID }),
    );
    dealAggregate.apply(new PersonRelatedToDealEvent(dealID, personID, meta));
    dealAggregate.commit();
  }
}
