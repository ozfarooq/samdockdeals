import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { DecisionMakerAssignedToLeadEvent } from '@daypaio/domain-events/deals';
import { AssignDecisionMakerToLeadCommand } from '../impl/assign-decision-maker-to-lead.command';

@CommandHandler(AssignDecisionMakerToLeadCommand)
export class AssignDecisionMakerToLeadCommandHandler
  implements ICommandHandler<AssignDecisionMakerToLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AssignDecisionMakerToLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, decisionMakerID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(new DecisionMakerAssignedToLeadEvent(_dealID, decisionMakerID, meta));
    dealAggregate.commit();
  }
}
