import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { CreateLeadFromDraftCommand } from '../impl/create-lead-from-draft.command';
import { LeadCreatedFromDraftEvent } from '@daypaio/domain-events/deals';

@CommandHandler(CreateLeadFromDraftCommand)
export class CreateLeadFromDraftCommandHandler
  implements ICommandHandler<CreateLeadFromDraftCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: CreateLeadFromDraftCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, _personID, _organizationID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.apply(
        new LeadCreatedFromDraftEvent(_dealID, _personID, _organizationID, meta),
    );
    dealAggregate.commit();
  }
}
