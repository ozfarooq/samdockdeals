import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { ChangeDealStageCommand } from '../impl/change-deal-stage.command';

@CommandHandler(ChangeDealStageCommand)
export class ChangeDealStageCommandHandler
  implements ICommandHandler<ChangeDealStageCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: ChangeDealStageCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, stage: status, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _dealID }),
    );
    dealAggregate.changeStage(status, meta);
    dealAggregate.commit();
  }
}
