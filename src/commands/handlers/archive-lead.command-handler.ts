import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EventPublisher } from 'nestjs-eventstore';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { LeadArchivedEvent } from '@daypaio/domain-events/deals';
import { ArchiveLeadCommand } from '../impl/archive-lead.command';

@CommandHandler(ArchiveLeadCommand)
export class ArchiveLeadCommandHandler
  implements ICommandHandler<ArchiveLeadCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
  ) { }

  async execute(command: ArchiveLeadCommand) {
    this.logger.log('COMMAND TRIGGERED...');
    const { _leadID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: _leadID }),
    );
    dealAggregate.apply(new LeadArchivedEvent(_leadID, meta));
    dealAggregate.commit();
  }

}
