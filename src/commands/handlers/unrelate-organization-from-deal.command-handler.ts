import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealAggregate } from '../../models/deal.aggregate';
import { UnrelateOrganizationFromDealCommand } from '../impl/unrelate-organization-from-deal.command';
import { OrganizationUnrelatedFromDealEvent } from '@daypaio/domain-events/deals';

@CommandHandler(UnrelateOrganizationFromDealCommand)
export class UnrelateOrganizationFromDealCommandHandler
  implements ICommandHandler<UnrelateOrganizationFromDealCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelateOrganizationFromDealCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { dealID, organizationID, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: dealID }),
    );
    dealAggregate.apply(new OrganizationUnrelatedFromDealEvent(dealID, organizationID, meta));
    dealAggregate.commit();
  }
}
