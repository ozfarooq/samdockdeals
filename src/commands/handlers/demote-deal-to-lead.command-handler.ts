import { CommandHandler } from '@nestjs/cqrs';
import { DemoteDealToLeadCommand } from '../impl/demote-deal-to-lead.command';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { DealAggregate } from '../../models/deal.aggregate';
import { DealDemotedToLeadEvent } from '@daypaio/domain-events/deals';

@CommandHandler(DemoteDealToLeadCommand)
export class DemoteDealToLeadCommandHandler {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: DemoteDealToLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { id, meta } = command;
    const dealAggregate = this.publisher.mergeObjectContext(
      new DealAggregate({ _id: id }),
    );
    dealAggregate.apply(new DealDemotedToLeadEvent(id, meta));
    dealAggregate.commit();
  }

}
