import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EditPipelineCommand } from '../impl/edit-pipeline.command';
import { EventPublisher } from 'nestjs-eventstore';
import { PipelineAggregate } from '../../models/pipeline.aggregate';
import { PipelineEditedEvent } from '@daypaio/domain-events/deals';

@CommandHandler(EditPipelineCommand)
export class EditPipelineCommandHandler
  implements ICommandHandler<EditPipelineCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: EditPipelineCommand) {
    this.logger.log('COMMAND TRIGGERED: EditPipelineCommandHandler...');
    const { id, pipeline, meta } = command;
    pipeline._id = id;
    const pipelineAggregate = this.publisher.mergeObjectContext(
      new PipelineAggregate(pipeline),
    );
    pipelineAggregate.apply(new PipelineEditedEvent(id, pipeline, meta));
    pipelineAggregate.commit();
  }
}
