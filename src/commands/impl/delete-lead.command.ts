import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DeleteLeadCommand implements ICommand {
  constructor(
    public readonly id: string,
    public readonly meta: EventMetaData,
  ) {}
}
