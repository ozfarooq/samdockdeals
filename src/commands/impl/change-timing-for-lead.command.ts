import { EventMetaData } from '@daypaio/domain-events/shared';
import { ICommand } from '@nestjs/cqrs';

export class ChangeTimingForLeadCommand implements ICommand {
  constructor(
        public _dealID: string,
        public timestamp: number,
        public meta: EventMetaData,
      ) {}
}
