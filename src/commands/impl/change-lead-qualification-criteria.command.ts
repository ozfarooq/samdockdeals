import { ICommand, IEvent } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ChangeLeadQualificationCriteriaCommand implements ICommand {
  constructor(
    public _dealID: string,
    public meta: EventMetaData,
  ) {}
}
