import { EventMetaData } from '@daypaio/domain-events/shared';
import { ICommand } from '@nestjs/cqrs';

export class RemoveDecisionMakerFromLeadCommand implements ICommand {
  constructor(
        public _dealID: string,
        public meta: EventMetaData,
      ) {}
}
