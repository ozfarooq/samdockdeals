import { ICommand } from '@nestjs/cqrs';
import { IDeal } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class AddDealCommand implements ICommand {
  constructor(
    public readonly deal: IDeal,
    public readonly meta: EventMetaData,
  ) {}
}
