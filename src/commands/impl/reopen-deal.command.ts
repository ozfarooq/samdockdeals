import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ReopenDealCommand implements ICommand {
  constructor(
    public readonly _dealID: string,
    public readonly pipelineID: string,
    public readonly stageID: string,
    public readonly meta: EventMetaData,
  ) {}
}
