import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class SortDealCommand implements ICommand {
  constructor(
    public dealID: string,
    public pipelineID: string,
    public stageID: string,
    public sortIndex: number,
    public meta: EventMetaData,
  ) { }
}
