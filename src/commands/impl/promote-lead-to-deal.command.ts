import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class PromoteLeadToDealCommand implements ICommand {
  constructor(
    public readonly id: string,
    public readonly pipelineID: string,
    public readonly meta: EventMetaData,
  ) {}
}
