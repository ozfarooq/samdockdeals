import { ICommand } from '@nestjs/cqrs';
import { IPipelineStage } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class AddPipelineStageCommand implements ICommand {
  constructor(
    public readonly pipelineID: string,
    public readonly stage: IPipelineStage,
    public readonly meta: EventMetaData,
  ) {}
}
