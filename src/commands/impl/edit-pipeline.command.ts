import { ICommand } from '@nestjs/cqrs';
import { IPipeline } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class EditPipelineCommand implements ICommand {
  constructor(
    public readonly id: string,
    public readonly pipeline: Partial<IPipeline>,
    public readonly meta: EventMetaData,
  ) {}
}
