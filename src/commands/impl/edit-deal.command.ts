import { ICommand } from '@nestjs/cqrs';
import { IDeal } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class EditDealCommand implements ICommand {
  constructor(
    public readonly id: string,
    public readonly deal: Partial<IDeal>,
    public readonly meta: EventMetaData,
  ) {}
}
