import { EventMetaData } from '@daypaio/domain-events/shared';
import { ICommand } from '@nestjs/cqrs';

export class AssignDecisionMakerToDealCommand implements ICommand {
  constructor(
        public _dealID: string,
        public decisionMakerID: string,
        public meta: EventMetaData,
      ) {}
}
