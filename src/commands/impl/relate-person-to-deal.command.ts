import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RelatePersonToDealCommand implements ICommand {
  constructor(
    public dealID: string,
    public personID: string,
    public meta: EventMetaData,
  ) {}
}
