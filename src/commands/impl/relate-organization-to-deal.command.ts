import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RelateOrganizationToDealCommand implements ICommand {
  constructor(
    public dealID: string,
    public organizationID: string,
    public meta: EventMetaData,
  ) {}
}
