import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RemovePipelineStageCommand implements ICommand {
  constructor(
    public readonly pipelineID: string,
    public readonly id: string,
    public readonly toStageID: string,
    public readonly meta: EventMetaData,
  ) {}
}
