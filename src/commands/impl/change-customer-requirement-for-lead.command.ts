import { EventMetaData } from '@daypaio/domain-events/shared';
import { ICommand } from '@nestjs/cqrs';

export class ChangeCustomerRequirementForLeadCommand implements ICommand {
  constructor(
        public _dealID: string,
        public customerRequirement: string,
        public meta: EventMetaData,
      ) {}
}
