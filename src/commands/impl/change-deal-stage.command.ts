import { ICommand } from '@nestjs/cqrs';
import { IDealStage } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ChangeDealStageCommand implements ICommand {
  constructor(
    public _dealID: string,
    public stage: IDealStage,
    public meta: EventMetaData,
  ) {}
}
