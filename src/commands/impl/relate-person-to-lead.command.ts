import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RelatePersonToLeadCommand implements ICommand {
  constructor(
    public leadID: string,
    public personID: string,
    public meta: EventMetaData,
  ) { }
}
