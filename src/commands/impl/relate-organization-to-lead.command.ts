import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RelateOrganizationToLeadCommand implements ICommand {
  constructor(
    public leadID: string,
    public organizationID: string,
    public meta: EventMetaData,
  ) { }
}
