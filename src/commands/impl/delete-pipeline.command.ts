import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DeletePipelineCommand implements ICommand {
  constructor(
    public id: string,
    public toPipelineID: string,
    public toStageID: string,
    public meta: EventMetaData,
  ) { }
}
