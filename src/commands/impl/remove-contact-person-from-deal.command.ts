import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RemoveContactPersonFromDealCommand implements ICommand {
  constructor(
    public _dealID: string,
    public _personID: string,
    public meta: EventMetaData,
  ) {}
}
