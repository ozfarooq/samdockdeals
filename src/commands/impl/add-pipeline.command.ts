import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IPipeline } from '@daypaio/domain-events/deals';

export class AddPipelineCommand implements ICommand {
  constructor(
    public pipeline: IPipeline,
    public meta: EventMetaData,
  ) { }
}
