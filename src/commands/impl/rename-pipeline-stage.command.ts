import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RenamePipelineStageCommand implements ICommand {
  constructor(
    public pipelineID: string,
    public stageID: string,
    public newName: string,
    public meta: EventMetaData,
  ) {}
}
