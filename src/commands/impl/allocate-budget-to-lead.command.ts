import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IDealBudget } from '@daypaio/domain-events/deals';

export class AllocateBudgetToLeadCommand implements ICommand {
  constructor(
        public _dealID: string,
        public budget: IDealBudget,
        public meta: EventMetaData,
      ) {}
}
