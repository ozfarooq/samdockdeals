import { EventMetaData } from '@daypaio/domain-events/shared';
import { ICommand } from '@nestjs/cqrs';

export class AssignBudgetHolderToLeadCommand implements ICommand {
  constructor(
        public _dealID: string,
        public budgetHolderID: string,
        public meta: EventMetaData,
      ) {}
}
