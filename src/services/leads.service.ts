import { Injectable } from '@nestjs/common';
import { BreadService } from '../shared/services/bread.service';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ReadDealQuery } from '../queries/impl/read-deal.query';
import { IDeal, IDealBudget } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { QualifyLeadCommand } from '../commands/impl/qualify-lead.command';
import { BrowseLeadsQuery } from '../queries/impl/browse-leads.query';
import { EditLeadCommand } from '../commands/impl/edit-lead.command';
import { AddLeadCommand } from '../commands/impl/add-lead.command';
import { DeleteLeadCommand } from '../commands/impl/delete-lead.command';
import { RelateOrganizationToLeadCommand } from '../commands/impl/relate-organization-to-lead.command';
import { UnrelateOrganizationFromLeadCommand } from '../commands/impl/unrelate-organization-from-lead.command';
import { RelatePersonToLeadCommand } from '../commands/impl/relate-person-to-lead.command';
import { UnrelatePersonFromLeadCommand } from '../commands/impl/unrelate-person-to-lead.command';
import { AssignContactPersonToLeadCommand } from '../commands/impl/assign-contact-person-to-lead.command';
import { RemoveContactPersonFromLeadCommand } from '../commands/impl/remove-contact-person-from-lead.command';
import { AllocateBudgetToLeadCommand } from '../commands/impl/allocate-budget-to-lead.command';
import { RemoveBudgetHolderFromLeadCommand } from '../commands/impl/remove-budget-holder-from-lead.command';
import { AssignBudgetHolderToLeadCommand } from '../commands/impl/assign-budget-holder-to-lead.command';
import { RemoveDecisionMakerFromLeadCommand } from '../commands/impl/remove-decision-maker-from-lead.command';
import { AssignDecisionMakerToLeadCommand } from '../commands/impl/assign-decision-maker-to-lead.command';
import { ChangeCustomerRequirementForLeadCommand } from '../commands/impl/change-customer-requirement-for-lead.command';
import { ChangeTimingForLeadCommand } from '../commands/impl/change-timing-for-lead.command';
import { UnqualifyLeadCommand } from '../commands/impl/unqualify-lead.command';
import { PromoteLeadToDealCommand } from '../commands/impl/promote-lead-to-deal.command';
import { ReopenLeadCommand } from '../commands/impl/reopen-lead.command';
import { ArchiveLeadCommand } from '../commands/impl/archive-lead.command';
import { BrowseAllDealsLeadsQuery } from '../queries/impl/browse-all-deals-leads.query';

@Injectable()
export class LeadsService extends BreadService<IDeal> {

  constructor(commandBus: CommandBus, queryBus: QueryBus) {
    super(commandBus, queryBus);
  }

  async browse(meta: EventMetaData): Promise<IDeal[]> {
    return await this.executeQuery(new BrowseLeadsQuery(meta));
  }

  async browseAll(meta: EventMetaData): Promise<IDeal[]> {
    return await this.executeQuery(new BrowseAllDealsLeadsQuery(meta));
  }

  async read(id: string, meta: EventMetaData): Promise<IDeal> {
    return await this.executeQuery(new ReadDealQuery(id, meta));
  }

  async edit(
    id: string,
    object: IDeal,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(
      new EditLeadCommand(id, object, meta),
    );
  }

  async add(object: IDeal, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(
      new AddLeadCommand(object, meta),
    );
  }

  async delete(id: string, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new DeleteLeadCommand(id, meta));
  }

  async deleteAll(meta: EventMetaData): Promise<void> {
    const items = await this.browse(meta);
    const ids = items.map(item => item._id);
    return this.bulkDelete(ids, meta);
  }

  async bulkDelete(ids: string[], meta: EventMetaData): Promise<void> {
    await Promise.all(ids.map(async (id) => {
      return await this.executeCommand(new DeleteLeadCommand(id, meta));
    }));
  }

  async relateOrganization(
    dealID: string,
    organizationID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new RelateOrganizationToLeadCommand(dealID, organizationID, meta),
    );
  }

  async unrelateOrganization(
    dealID: string,
    organizationID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new UnrelateOrganizationFromLeadCommand(dealID, organizationID, meta),
    );
  }

  async relatePerson(
    dealID: string,
    personID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new RelatePersonToLeadCommand(dealID, personID, meta),
    );
  }

  async unrelatePerson(
    dealID: string,
    personID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new UnrelatePersonFromLeadCommand(dealID, personID, meta),
    );
  }

  async assignContactPerson(
    dealID: string,
    personID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new AssignContactPersonToLeadCommand(dealID, personID, meta),
    );
  }

  async removeContactPerson(
    dealID: string,
    personID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new RemoveContactPersonFromLeadCommand(dealID, personID, meta),
    );
  }

  async unqualify(
    leadID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new UnqualifyLeadCommand(leadID, meta),
    );
  }

  async qualify(
    leadID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new QualifyLeadCommand(leadID, meta),
    );
  }

  async promote(
    leadID: string,
    pipelineID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new PromoteLeadToDealCommand(leadID, pipelineID, meta),
    );
  }

  async changeBudget(
    dealID: string,
    budget: IDealBudget,
    meta: EventMetaData,
  ) {
    return this.executeCommand(new AllocateBudgetToLeadCommand(dealID, budget, meta));
  }

  async changeBudgetHolder(
    dealID: string,
    budgetHolderID: string,
    meta: EventMetaData,
  ) {
    if (!budgetHolderID) {
      return this.executeCommand(
        new RemoveBudgetHolderFromLeadCommand(dealID, meta),
      );
    }
    return this.executeCommand(new AssignBudgetHolderToLeadCommand(
      dealID, budgetHolderID, meta,
    ));
  }

  async changeDecisionMaker(
    dealID: string,
    decisionMakerID: string,
    meta: EventMetaData,
  ) {
    if (!decisionMakerID) {
      return this.executeCommand(
        new RemoveDecisionMakerFromLeadCommand(dealID, meta),
      );
    }
    return this.executeCommand(new AssignDecisionMakerToLeadCommand(
      dealID, decisionMakerID, meta,
    ));
  }

  async changeCustomerRequirement(
    dealID: string,
    customerRequirement: string,
    meta: EventMetaData,
  ) {
    return this.executeCommand(new ChangeCustomerRequirementForLeadCommand(
      dealID, !!customerRequirement ? customerRequirement : null, meta,
    ));
  }

  async changeTiming(
    dealID: string,
    timing: number,
    meta: EventMetaData,
  ) {
    return this.executeCommand(
      new ChangeTimingForLeadCommand(dealID, !!timing ? timing : null, meta),
    );
  }

  async archive(leadID: string, meta: EventMetaData) {
    return this.executeCommand(new ArchiveLeadCommand(leadID, meta));
  }

  async reopen(leadID: string, meta: EventMetaData) {
    return this.executeCommand(new ReopenLeadCommand(leadID, meta));
  }

}
