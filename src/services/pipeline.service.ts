import { Injectable, NotFoundException, ForbiddenException, ConflictException } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { BrowsePipelinesQuery } from '../queries/impl/browse-pipelines.query';
import { RenamePipelineStageCommand } from '../commands/impl/rename-pipeline-stage.command';
import { Pipeline, FIRST_STAGE_ID, LOST_STAGE_ID, WON_STAGE_ID } from '../models/pipeline.model';
import { DeletePipelineCommand } from '../commands/impl/delete-pipeline.command';
import { SortDealCommand } from '../commands/impl/sort-deal.command';
import { EditPipelineCommand } from '../commands/impl/edit-pipeline.command';
import { AddPipelineStageCommand } from '../commands/impl/add-pipeline-stage.command';
import { RemovePipelineStageCommand } from '../commands/impl/remove-pipeline-stage.command';
import { IPipeline, IPipelineStage } from '@daypaio/domain-events/deals';
import { ReadPipelineQuery } from '../queries/impl/read-pipeline.query';
import { AddPipelineCommand } from '../commands/impl/add-pipeline.command';

const DEFAULT_STAGE_IDS = [FIRST_STAGE_ID, WON_STAGE_ID, LOST_STAGE_ID];

@Injectable()
export class PipelineService {
  constructor(private commandBus: CommandBus, private queryBus: QueryBus) {
  }

  read(id: string, meta: EventMetaData): Promise<Pipeline> {
    return this.queryBus.execute(new ReadPipelineQuery(id, meta));
  }

  add(pipeline: IPipeline, meta: EventMetaData) {
    this.commandBus.execute(new AddPipelineCommand(pipeline, meta));
  }

  browse(meta: EventMetaData): Promise<Pipeline[]> {
    return this.queryBus.execute(new BrowsePipelinesQuery(meta));
  }

  async changeStagePosition(pipelineID: string, stageID: string, newPosition: number, meta) {
    if (DEFAULT_STAGE_IDS.includes(stageID)) {
      throw new ForbiddenException(`Stage id "${stageID}" could not be default column`);
    }
    const pipeline = await this.read(pipelineID, meta);
    const stages = [...pipeline.stages];
    const stageIndex = stages.findIndex(item => item._id === stageID);
    if (stageIndex === -1) {
      throw new NotFoundException(`Stage id "${stageID}" is not found`);
    }
    const stage = stages.splice(stageIndex, 1)[0];
    stages.splice(newPosition, 0, stage);
    return this.edit(pipeline._id, { stages }, meta);
  }

  edit(id: string, data: Partial<IPipeline>, meta: EventMetaData) {
    return this.commandBus.execute(new EditPipelineCommand(id, data, meta));
  }

  async addStage(pipelineID: string, stage: IPipelineStage, meta: EventMetaData) {
    const pipeline = await this.read(pipelineID, meta);
    if (!pipeline) {
      throw new NotFoundException(`Pipeline with that id:${meta._tenantID} is not found`);
    }
    if (pipeline.stages.some(item => item._id === stage._id)) {
      throw new ConflictException(`Stage with that id:${stage._id} is already exists`);
    }
    return this.commandBus.execute(new AddPipelineStageCommand(pipelineID, stage, meta));
  }

  async removeStage(pipelineID: string, id: string, toStageID: string, meta: EventMetaData) {
    if (DEFAULT_STAGE_IDS.includes(id)) {
      throw new ForbiddenException(`Stage id:${id} could not be removed, because it's a default column`);
    }
    return this.commandBus.execute(new RemovePipelineStageCommand(pipelineID, id, toStageID, meta));
  }

  renamePipelineStage(pipelineID: string, stageID: string, newName: string, meta: EventMetaData) {
    return this.commandBus.execute(
      new RenamePipelineStageCommand(pipelineID, stageID, newName, meta),
    );
  }

  sortDeal(
    pipelineID: string,
    stageID: string,
    dealID: string,
    sortIndex: number,
    meta: EventMetaData,
  ) {
    return this.commandBus.execute(
      new SortDealCommand(dealID, pipelineID, stageID, sortIndex, meta),
    );
  }

  async delete(
    id: string,
    toPipelineID: string,
    toStageID: string,
    meta: EventMetaData,
  ): Promise<void> {
    return this.commandBus.execute(
      new DeletePipelineCommand(id, toPipelineID, toStageID, meta),
    );
  }

}
