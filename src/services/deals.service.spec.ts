import { Test, TestingModule } from '@nestjs/testing';
import { DealsService } from './deals.service';
import { CqrsModule, CommandBus, QueryBus, EventBus } from '@nestjs/cqrs';
import { DealCommandHandlers } from '../commands/handlers';
import { DealQueryHandlers } from '../queries/handlers';
import { DealRepository } from '../repositories/deals.repository';
import { AddDealCommandHandler } from '../commands/handlers/add-deal.command-handler';
import { EditDealCommandHandler } from '../commands/handlers/edit-deal.command-handler';
import { DeleteDealCommandHandler } from '../commands/handlers/delete-deal.command-handler';
import { ReadDealHandler } from '../queries/handlers/read-deal.query-handler';
import { BrowseDealsHandler } from '../queries/handlers/browse-deals.query-handler';
import { EventBusProvider } from 'nestjs-eventstore';
import { DealDTO } from '../dto/deal.dto';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { PipelineRepository } from '../repositories/pipeline.repository';

const mockDealRepository = () => ({
  create: jest.fn(),
});

describe('DealsService', () => {
  let service: DealsService;
  let commandBus: CommandBus;
  let queryBus: QueryBus;
  const mockMeta = new EventMetaData('mocktenant', 'xyz');
  let addDealCommandHandler: AddDealCommandHandler;
  let editDealCommandHandler: EditDealCommandHandler;
  let deleteDealCommandHandler: DeleteDealCommandHandler;
  let browseDealQueryHandler: BrowseDealsHandler;
  let readDealQueryHandler: ReadDealHandler;

  const mockDeal = {
    _id: 'a',
    name: 'taimoor',
    email: 'this@example.com',
    phoneNumber: 1232341432,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        DealsService,
        ...DealCommandHandlers,
        ...DealQueryHandlers,
        {
          provide: EventBusProvider,
          useExisting: EventBus,
        },
        {
          provide: DealRepository,
          useFactory: mockDealRepository,
        },
        {
          provide: PipelineRepository,
          useFactory: mockDealRepository,
        },
      ],
    }).compile();

    service = module.get<DealsService>(DealsService);
    commandBus = module.get<CommandBus>(CommandBus);
    queryBus = module.get<QueryBus>(QueryBus);
    addDealCommandHandler = module.get<AddDealCommandHandler>(
      AddDealCommandHandler,
    );
    editDealCommandHandler = module.get<EditDealCommandHandler>(
      EditDealCommandHandler,
    );
    deleteDealCommandHandler = module.get<
      DeleteDealCommandHandler>(DeleteDealCommandHandler);
    browseDealQueryHandler = module.get<BrowseDealsHandler>(
      BrowseDealsHandler,
    );
    readDealQueryHandler = module.get<ReadDealHandler>(
      ReadDealHandler,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('commands', () => {
    describe('for executing commands on the command bus', () => {
      it('should be executed on the commandBus for adding deal', async () => {
        jest.spyOn(commandBus, 'execute').mockResolvedValue(null);
        expect(commandBus.execute).not.toHaveBeenCalled();
        await service.add(
          new DealDTO(mockDeal),
          mockMeta,
        );
        expect(commandBus.execute).toHaveBeenCalled();
      });

      it('should be executed on the commandBus for editing deal', async () => {
        jest.spyOn(commandBus, 'execute').mockResolvedValue(null);
        expect(commandBus.execute).not.toHaveBeenCalled();
        await service.edit(
          mockDeal._id,
          new DealDTO(mockDeal),
          mockMeta,
        );
        expect(commandBus.execute).toHaveBeenCalled();
      });

      it('should be executed on the commandBus for deleting deal', async () => {
        jest.spyOn(commandBus, 'execute').mockResolvedValue(null);
        expect(commandBus.execute).not.toHaveBeenCalled();
        await service.delete(mockDeal._id, mockMeta);
        expect(commandBus.execute).toHaveBeenCalled();
      });
    });

    describe('for triggering command handlers', () => {
      it('should trigger AddDealCommand when adding an deal', async () => {
        jest
          .spyOn(addDealCommandHandler, 'execute')
          .mockReturnValue(null);
        commandBus.register([AddDealCommandHandler]);
        expect(addDealCommandHandler.execute).not.toHaveBeenCalled();
        await service.add(
          new DealDTO(mockDeal),
          mockMeta,
        );
        expect(addDealCommandHandler.execute).toHaveBeenCalled();
      });

      it('should trigger EditDealCommand when editing an deal', async () => {
        jest
          .spyOn(editDealCommandHandler, 'execute')
          .mockReturnValue(null);
        commandBus.register([EditDealCommandHandler]);
        expect(editDealCommandHandler.execute).not.toHaveBeenCalled();
        await service.edit(
          mockDeal._id,
          new DealDTO(mockDeal),
          mockMeta,
        );
        expect(editDealCommandHandler.execute).toHaveBeenCalled();
      });

      it('should trigger DeleteDealCommand when deleting an deal', async () => {
        jest
          .spyOn(deleteDealCommandHandler, 'execute')
          .mockReturnValue(null);
        commandBus.register([DeleteDealCommandHandler]);
        expect(deleteDealCommandHandler.execute).not.toHaveBeenCalled();
        await service.delete(mockDeal._id, mockMeta);
        expect(deleteDealCommandHandler.execute).toHaveBeenCalled();
      });
    });
  });

  describe('queries', () => {
    describe('for executing queries on the QueryBus', () => {
      it('should be executed on the queryBus for browsing deals', async () => {
        jest.spyOn(queryBus, 'execute').mockResolvedValue(null);
        expect(queryBus.execute).not.toHaveBeenCalled();
        await service.browse(mockMeta);
        expect(queryBus.execute).toHaveBeenCalled();
      });

      it('should be executed on the queryBus for finding an deal', async () => {
        jest.spyOn(queryBus, 'execute').mockResolvedValue(null);
        expect(queryBus.execute).not.toHaveBeenCalled();
        await service.read(mockDeal._id, mockMeta);
        expect(queryBus.execute).toHaveBeenCalled();
      });
    });

    describe('for triggering query handlers', () => {
      it('should trigger BrowseDealQuery when browsing deals', async () => {
        jest
          .spyOn(browseDealQueryHandler, 'execute')
          .mockReturnValue(null);
        queryBus.register([BrowseDealsHandler]);
        expect(browseDealQueryHandler.execute).not.toHaveBeenCalled();
        await service.browse(mockMeta);
        expect(browseDealQueryHandler.execute).toHaveBeenCalled();
      });

      it('should trigger ReadDealQuery when reading  an deals', async () => {
        jest
          .spyOn(readDealQueryHandler, 'execute')
          .mockReturnValue(null);
        queryBus.register([ReadDealHandler]);
        expect(readDealQueryHandler.execute).not.toHaveBeenCalled();
        await service.read(mockDeal._id, mockMeta);
        expect(readDealQueryHandler.execute).toHaveBeenCalled();
      });
    });
  });
});
