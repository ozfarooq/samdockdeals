import { Injectable } from '@nestjs/common';
import { BreadService } from '../shared/services/bread.service';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { BrowseDealsQuery } from '../queries/impl/browse-deals.query';
import { ReadDealQuery } from '../queries/impl/read-deal.query';
import { EditDealCommand } from '../commands/impl/edit-deal.command';
import { AddDealCommand } from '../commands/impl/add-deal.command';
import { DeleteDealCommand } from '../commands/impl/delete-deal.command';
import { IDeal, IDealStage, IDealBudget } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { RelateOrganizationToDealCommand } from '../commands/impl/relate-organization-to-deal.command';
import { UnrelateOrganizationFromDealCommand } from '../commands/impl/unrelate-organization-from-deal.command';
import { RelatePersonToDealCommand } from '../commands/impl/relate-person-to-deal.command';
import { UnrelatePersonFromDealCommand } from '../commands/impl/unrelate-person-from-deal.command';
import { AssignContactPersonToDealCommand } from '../commands/impl/assign-contact-person-to-deal.command';
import { RemoveContactPersonFromDealCommand } from '../commands/impl/remove-contact-person-from-deal.command';
import { ChangeDealStageCommand } from '../commands/impl/change-deal-stage.command';
import { AllocateBudgetToDealCommand } from '../commands/impl/allocate-budget-to-deal.command';
import { RemoveDecisionMakerFromDealCommand } from '../commands/impl/remove-decision-maker-from-deal.command';
import { AssignDecisionMakerToDealCommand } from '../commands/impl/assign-decision-maker-to-deal.command';
import { ChangeCustomerRequirementForDealCommand } from '../commands/impl/change-customer-requirement-for-deal.command';
import { ChangeTimingForDealCommand } from '../commands/impl/change-timing-for-deal.command';
import { RemoveBudgetHolderFromDealCommand } from '../commands/impl/remove-budget-holder-from-deal.command';
import { AssignBudgetHolderToDealCommand } from '../commands/impl/assign-budget-holder-to-deal.command';
import { DemoteDealToLeadCommand } from '../commands/impl/demote-deal-to-lead.command';
import { ArchiveDealCommand } from '../commands/impl/archive-deal.command';
import { ReopenDealCommand } from '../commands/impl/reopen-deal.command';

@Injectable()
export class DealsService extends BreadService<IDeal> {
  constructor(commandBus: CommandBus, queryBus: QueryBus) {
    super(commandBus, queryBus);
  }

  async browse(meta: EventMetaData): Promise<IDeal[]> {
    return await this.executeQuery(new BrowseDealsQuery(meta));
  }

  async read(id: string, meta: EventMetaData): Promise<IDeal> {
    return await this.executeQuery(new ReadDealQuery(id, meta));
  }

  async edit(id: string, object: IDeal, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new EditDealCommand(id, object, meta));
  }

  async add(object: IDeal, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new AddDealCommand(object, meta));
  }

  async delete(id: string, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new DeleteDealCommand(id, meta));
  }

  async deleteAll(meta: EventMetaData): Promise<void> {
    const items = await this.browse(meta);
    const ids = items.map(item => item._id);
    return this.bulkDelete(ids, meta);
  }

  async bulkDelete(ids: string[], meta: EventMetaData): Promise<void> {
    await Promise.all(
      ids.map(async (id) => {
        return await this.executeCommand(new DeleteDealCommand(id, meta));
      }),
    );
  }

  async relateOrganization(
    dealID: string,
    organizationID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new RelateOrganizationToDealCommand(dealID, organizationID, meta),
    );
  }

  async unrelateOrganization(
    dealID: string,
    organizationID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new UnrelateOrganizationFromDealCommand(dealID, organizationID, meta),
    );
  }

  async relatePerson(dealID: string, personID: string, meta: EventMetaData) {
    return await this.executeCommand(
      new RelatePersonToDealCommand(dealID, personID, meta),
    );
  }

  async unrelatePerson(dealID: string, personID: string, meta: EventMetaData) {
    return await this.executeCommand(
      new UnrelatePersonFromDealCommand(dealID, personID, meta),
    );
  }

  async assignContactPerson(
    dealID: string,
    personID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new AssignContactPersonToDealCommand(dealID, personID, meta),
    );
  }

  async removeContactPerson(
    dealID: string,
    personID: string,
    meta: EventMetaData,
  ) {
    return await this.executeCommand(
      new RemoveContactPersonFromDealCommand(dealID, personID, meta),
    );
  }

  async changeStage(dealID: string, stage: IDealStage, meta: EventMetaData) {
    return await this.executeCommand(
      new ChangeDealStageCommand(dealID, stage, meta),
    );
  }

  async demote(dealID: string, meta: EventMetaData) {
    return await this.executeCommand(new DemoteDealToLeadCommand(dealID, meta));
  }

  async changeBudget(dealID: string, budget: IDealBudget, meta: EventMetaData) {
    return this.executeCommand(
      new AllocateBudgetToDealCommand(dealID, budget, meta),
    );
  }

  async changeBudgetHolder(
    dealID: string,
    budgetHolderID: string,
    meta: EventMetaData,
  ) {
    if (!budgetHolderID) {
      return this.executeCommand(
        new RemoveBudgetHolderFromDealCommand(dealID, meta),
      );
    }
    return this.executeCommand(
      new AssignBudgetHolderToDealCommand(dealID, budgetHolderID, meta),
    );
  }

  async changeDecisionMaker(
    dealID: string,
    decisionMakerID: string,
    meta: EventMetaData,
  ) {
    if (!decisionMakerID) {
      return this.executeCommand(
        new RemoveDecisionMakerFromDealCommand(dealID, meta),
      );
    }
    return this.executeCommand(
      new AssignDecisionMakerToDealCommand(dealID, decisionMakerID, meta),
    );
  }

  async changeCustomerRequirement(
    dealID: string,
    customerRequirement: string,
    meta: EventMetaData,
  ) {
    return this.executeCommand(
      new ChangeCustomerRequirementForDealCommand(
        dealID,
        !!customerRequirement ? customerRequirement : null,
        meta,
      ),
    );
  }

  async changeTiming(dealID: string, timing: number, meta: EventMetaData) {
    return this.executeCommand(
      new ChangeTimingForDealCommand(dealID, !!timing ? timing : null, meta),
    );
  }

  async archive(dealID: string, meta: EventMetaData) {
    return this.executeCommand(new ArchiveDealCommand(dealID, meta));
  }

  async reopen(dealID: string, pipelineID, stageID: string, meta: EventMetaData) {
    return this.executeCommand(new ReopenDealCommand(dealID, pipelineID, stageID, meta));
  }
}
