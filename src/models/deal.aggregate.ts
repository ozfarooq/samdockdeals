import {
  IDeal,
  IDealPosition,
  IDealStage,
  DealWonEvent,
  DealLostEvent,
  DealStageChangedEvent,
  IDealBudget,
  IDealTiming,
  IDealBudgetType,
} from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IUser } from '@daypaio/domain-events/users';
import { IOrganization } from '@daypaio/domain-events/organizations';
import { AggregateRoot } from '@nestjs/cqrs';
import { plainToClassFromExist } from 'class-transformer';
import { LOST_STAGE_ID, WON_STAGE_ID } from './pipeline.model';

export class DealAggregate extends AggregateRoot implements IDeal {

  _id: string;

  name: string;

  positions: IDealPosition[];

  value: number;

  positionsEstimated: number;

  organization: Partial<IOrganization>;

  editor: IUser;

  stage: IDealStage;

  stakeholders: string[];

  contactPerson: string;

  budget: IDealBudget;

  customerRequirement: string;

  timing: IDealTiming;

  budgetHolder: string;

  decisionMaker: string;

  isArchived: boolean;

  createdAt: number;

  constructor(data?: Partial<IDeal>) {
    super();
    plainToClassFromExist(this, data);
  }

  get isQualified() {
    return !!this.timing?.date &&
    !!this.customerRequirement &&
    (!!this.budgetHolder || !!this.decisionMaker) &&
    this.budget?.type === IDealBudgetType.Yes;
  }

  changeStage(dealStage: IDealStage, meta: EventMetaData) {
    if (dealStage.stageID === WON_STAGE_ID) {
      return this.apply(new DealWonEvent(this._id, dealStage, meta));
    }
    if (dealStage.stageID === LOST_STAGE_ID) {
      return this.apply(new DealLostEvent(this._id, dealStage, meta));
    }
    return this.apply(new DealStageChangedEvent(this._id, dealStage, meta));
  }

}
