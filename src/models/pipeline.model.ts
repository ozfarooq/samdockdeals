import { index, prop } from '@typegoose/typegoose';
import { ApiProperty } from '@nestjs/swagger';
import { plainToClassFromExist } from 'class-transformer';
import { IPipeline, IPipelineStage } from '@daypaio/domain-events/deals';
import { nanoid } from 'nanoid';

export const WON_STAGE_ID = 'won';
export const LOST_STAGE_ID = 'lost';
export const QUALIFIED_STAGE_ID = 'qualified';
export const PROSPECTING_STAGE_ID = 'prospecting';
export const FIRST_STAGE_ID = 'first';

export class PipelineStage implements IPipelineStage {
  @ApiProperty({
    description: 'The ID of the stage',
    required: true,
    default: nanoid(),
  })
  @prop({ default: nanoid(), required: true })
  _id: string;

  @ApiProperty({
    description: 'Custom name of the stage',
    required: true,
  })
  @prop({ required: true })
  name: string;

  @ApiProperty({
    description: 'Deal ids of this stage',
    isArray: true,
    default: [],
    type: String,
  })
  @prop({ type: String, default: [] })
  order: string[];
}

@index({ _tenantID: 1 })
@index({ _tenantID: 1, _id: 1 })
export class Pipeline implements IPipeline {
  @ApiProperty({
    description: 'The ID of the pipeline',
    required: true,
  })
  @prop({ required: true })
  _id: string;

  @prop({ required: true })
  _tenantID: string;

  @prop({ required: true })
  name: string;

  @ApiProperty({
    description: 'Pipeline stages',
    type: PipelineStage,
    isArray: true,
  })
  @prop({ type: PipelineStage })
  stages: PipelineStage[];

  @prop()
  createdAt: number;

  constructor(data?: Partial<Pipeline>) {
    plainToClassFromExist(this, data);
  }
}
