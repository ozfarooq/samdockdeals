import { prop, index } from '@typegoose/typegoose';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { plainToClassFromExist } from 'class-transformer';
import { IDeal, IDealPosition, IDealBudget, IDealTiming, IDealStage } from '@daypaio/domain-events/deals';
import { IUser } from '@daypaio/domain-events/users';
import { IOrganization } from '@daypaio/domain-events/organizations';

export class DealPosition implements IDealPosition {
  @prop()
  _id: string;

  @prop()
  name: string;

  @prop()
  value: number;

  @prop()
  qty: number;

  @prop()
  unit: 'pieces' | 'hours';

  @prop()
  tax: number;
}

@index({ _tenantID: 1 })
@index({ _tenantID: 1, 'stage.stageID': 1 })
@index({ _tenantID: 1, _id: 1 })
export class Deal implements IDeal {
  @ApiProperty({
    description: 'the related tenant',
    required: true,
  })
  @prop({ required: true })
  _tenantID: string;

  @ApiProperty({
    description: 'The ID of the deal',
    required: true,
  })
  @prop({ required: true, unique: true })
  _id: string;

  @ApiPropertyOptional({
    description: 'The name of the deal',
  })
  @prop()
  name: string;

  @ApiProperty({
    description: 'the organization the deal is linked to',
  })
  @prop({ required: false })
  organization: Partial<IOrganization>;

  @ApiPropertyOptional({
    description: 'the different positions in the deal',
  })
  @prop({ type: DealPosition, _id: false })
  positions: IDealPosition[];

  @ApiPropertyOptional({
    description: 'the value of the deal',
  })
  @prop()
  value: number;

  @ApiPropertyOptional({
    description: 'Estimated value',
  })
  @prop({ required: false })
  positionsEstimated: number;

  @ApiPropertyOptional({
    description: 'The editor of the deal',
  })
  @prop({ required: false })
  editor: IUser;

  @ApiPropertyOptional({
    description: 'The stage of the deal',
  })
  @prop({ required: false })
  stage: IDealStage;

  @ApiPropertyOptional({
    description: 'The stakeholders of the deal',
    isArray: true,
  })
  @prop({ required: false })
  stakeholders: string[];

  @ApiPropertyOptional({
    description: 'The contact person of the deal',
  })
  @prop({ required: false })
  contactPerson: string;

  @ApiPropertyOptional({
    description: 'The budget of the deal or the lead',
  })
  @prop({ required: false })
  budget: IDealBudget;

  @ApiPropertyOptional({
    description: 'The customer requirement of the deal of the lead',
  })
  @prop({ required: false })
  customerRequirement: string;

  @ApiPropertyOptional({
    description: 'The timing property of the lead or the deal',
  })
  @prop({ required: false })
  timing: IDealTiming;

  @ApiPropertyOptional({
    description: 'The budget holder of the lead',
  })
  @prop({ required: false })
  budgetHolder: string;

  @ApiPropertyOptional({
    description: 'The decision maker of the lead',
  })
  @prop({ required: false })
  decisionMaker: string;

  @ApiPropertyOptional({
    description: 'The archived state of the deal',
  })
  @prop({ required: false })
  isArchived: boolean;

  @ApiPropertyOptional({
    description: 'The created at timestamp of the deal',
  })
  @prop({ required: false })
  createdAt: number;

  @ApiPropertyOptional({
    description: 'The updated at timestamp of the deal',
  })
  @prop({ required: false })
  updatedAt: number;

  constructor(data?: Partial<IDeal>) {
    plainToClassFromExist(this, data);
  }
}
