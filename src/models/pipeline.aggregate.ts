import {
  IPipeline,
  PipelineStageRenamedEvent,
  PipelineDeletedEvent,
} from '@daypaio/domain-events/deals';
import { AggregateRoot } from '@nestjs/cqrs';
import { plainToClassFromExist } from 'class-transformer';
import { PipelineStage } from './pipeline.model';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class PipelineAggregate extends AggregateRoot implements IPipeline {

  _id: string;

  name: string;

  stages: PipelineStage[];

  createdAt: number;

  constructor(data?: Partial<IPipeline>) {
    super();
    plainToClassFromExist(this, data);
  }

  renamePipelineStage(
    pipelineID: string,
    pipelineStageID: string,
    newName: string,
    meta: EventMetaData,
  ) {
    this.apply(new PipelineStageRenamedEvent(pipelineID, pipelineStageID, newName, meta));
  }

  delete(toPipelineID: string, toStageID: string, meta: EventMetaData) {
    this.apply(new PipelineDeletedEvent(this._id, toPipelineID, toStageID, meta));
  }

}
