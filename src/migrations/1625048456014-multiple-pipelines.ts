import { Logger } from '@nestjs/common';
import { getModelForClass, mongoose, ReturnModelType } from '@typegoose/typegoose';
import { setupDotenv } from '../dotenv.setup';
import { Pipeline } from '../models/pipeline.model';
import { MigrationInterface } from 'typeorm';
import { Deal } from '../models/deal.model';

export class MultiplePipelines1625048456014 implements MigrationInterface {
  protected logger: Logger;
  protected readonly dealModel: ReturnModelType<typeof Deal, {}>;
  protected readonly pipelineModel: ReturnModelType<typeof Pipeline, {}>;

  constructor() {
    this.dealModel = getModelForClass(Deal);
    this.pipelineModel = getModelForClass(Pipeline);
    this.logger = new Logger(`${this.constructor.name}`);
  }

  public async up(): Promise<any> {
    setupDotenv();
    mongoose.set('debug', true);

    await mongoose.connect(process.env.MONGO_CONNECTION_STRING);

    await mongoose.connection.db
      .collection('boardsettings')
      .rename('pipelines', { dropTarget: true });

    this.logger.log('Running migration...');

    await this.dealModel.updateMany(
      {},
      [
        {
          $set: {
            'stage.pipelineID': '$_tenantID',
            'stage.stageID': '$status._id',
          },
        },
        {
          $unset: ['status'],
        },
      ],
    );

    await this.dealModel.updateMany(
      { 'stage.stageID': 'lost' },
      { $set: { isArchived: true } },
    );

    await this.pipelineModel.updateMany(
      { },
      [
        {
          $set: {
            _tenantID: '$_id',
            name: 'Pipeline',
            createdAt: Date.now(),
            stages: '$columns',
          },
        },
        {
          $unset: ['columns'],
        },
      ],
    );

    await this.pipelineModel.updateMany(
      { },
      { $pull: { stages: { _id: 'lost' } } },
    );

    await this.pipelineModel.syncIndexes();

    this.logger.log('Migration is done');
  }

  public async down(): Promise<void> {}
}
